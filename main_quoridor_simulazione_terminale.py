#script per testare il gioco nel terminale computer contro computer

from moduli.Schema import Schema, const_casella_vuota
from moduli.Giocatore import Giocatore, const_numero_giocatore_computer, const_numero_giocatore_umano, const_numero_giocatore_disabilitato, const_numero_muri_restanti_infinito, const_prediligi_movimenti, const_non_prediligere_niente, const_prediligi_muri
from moduli.Mossa import const_tipologia_mossa_muro, const_tipologia_mossa_spostamento

import numpy as np

from termcolor import colored

dim_v = 10
dim_o = 30

numero_turni = 1000
profondita = 1
flag_analisi_sempre_muri = False
numero_muri = const_numero_muri_restanti_infinito
modalita_privilegio = const_prediligi_muri

stringa_continuare = ''
while len(stringa_continuare) == 0:

	#creo i giocatori
	g1 = Giocatore()
	g2 = Giocatore()
	g3 = Giocatore()
	g4 = Giocatore()

	g1.popola_attributi(id_giocatore=0, posizione_partenza=(0, dim_o//2), dim_v=dim_v, dim_o=dim_o, colore=colored(' ', 'blue', 'on_blue'), tipologia_giocatore=const_numero_giocatore_computer, muri_restanti=numero_muri)
	g2.popola_attributi(id_giocatore=1, posizione_partenza=(dim_v//2, 0), dim_v=dim_v, dim_o=dim_o, colore=colored(' ', 'white', 'on_white'), tipologia_giocatore=const_numero_giocatore_computer, muri_restanti=numero_muri)
	#g3.popola_attributi(id_giocatore=2, posizione_partenza=(dim_v-1, dim_o//2), dim_v=dim_v, dim_o=dim_o, colore=colored(' ', 'green', 'on_green'), tipologia_giocatore=const_numero_giocatore_computer, muri_restanti=numero_muri)
	g3.popola_giocatore_disabilitato(id_giocatore=2, colore=colored(' ', 'green', 'on_green'))
	#g4.popola_attributi(id_giocatore=3, posizione_partenza=(dim_v//2, dim_o-1), dim_v=dim_v, dim_o=dim_o, colore=colored(' ', 'yellow', 'on_yellow'), tipologia_giocatore=const_numero_giocatore_computer, muri_restanti=numero_muri)
	g4.popola_giocatore_disabilitato(id_giocatore=3, colore=colored(' ', 'yellow', 'on_yellow'))

	lista_giocatori = [g1, g2, g3, g4]

	#creo lo schema
	sc = np.ones((dim_v, dim_o), int)*const_casella_vuota

	schema_gioco = Schema(schema=sc)
	schema_gioco.popola_lista_direzioni(flag_diagonali=False)
	#lo stampo subito
	schema_gioco.stampa_schema(lista_giocaotri=lista_giocatori)

	#gioco per un numero di turni
	i = -1
	#while all( [ g.posizione not in g.posizioni_vittoria for g in lista_giocatori ] ):
	#while any( [ g.posizione not in g.posizioni_vittoria for g in lista_giocatori ] ):
	while sum([ 1 for g in lista_giocatori if ( (g.tipo_giocatore != const_numero_giocatore_disabilitato) and (g.posizione not in g.posizioni_vittoria)) ]) > 1:
	#for i in range(numero_turni):
		i += 1
		#dico di chi e il turno
		giocatore_in_turno = lista_giocatori[ i%4 ]
		if giocatore_in_turno.tipo_giocatore != const_numero_giocatore_disabilitato:
			print(f'Turno: {i+1}, giocatore: {giocatore_in_turno.id_giocatore}{giocatore_in_turno.colore}')
		if ((giocatore_in_turno.tipo_giocatore != const_numero_giocatore_disabilitato) and (giocatore_in_turno.posizione not in giocatore_in_turno.posizioni_vittoria)):
			#mi calcolo la sua mossa
			mossa_scelta = giocatore_in_turno.calcola_mossa_migliore_min_max(schema_partenza=schema_gioco, lista_giocatori=lista_giocatori, profondita=profondita, flag_controlla_sempre_muri=flag_analisi_sempre_muri, modalita_prediligi=const_prediligi_muri)
			#adesso stabilisco se e uno spostamento o un muro
			if mossa_scelta.tipologia_mossa == const_tipologia_mossa_spostamento:
				#devo cambiare la lista dei giocatori
				lista_giocatori = lista_giocatori.copy()
				nuovo_giocaotre = giocatore_in_turno.copy(nuova_posizione=mossa_scelta.spostamento[1], muri_restanti=giocatore_in_turno.muri_restanti, flag_tutti_campi=True)
				lista_giocatori[giocatore_in_turno.id_giocatore] = nuovo_giocaotre
			elif mossa_scelta.tipologia_mossa == const_tipologia_mossa_muro:
				#devo modifciare solo lo shcmea
				schema_gioco = schema_gioco.inserisci_muro(posizione_muro=mossa_scelta.muro)
				if numero_muri != const_numero_muri_restanti_infinito:
					#e la lista aggiornata dei giocatori
					lista_giocatori = lista_giocatori.copy()
					nuovo_giocaotre = giocatore_in_turno.copy(nuova_posizione=giocatore_in_turno.posizione, muri_restanti=giocatore_in_turno.muri_restanti-1, flag_tutti_campi=True)
					lista_giocatori[giocatore_in_turno.id_giocatore] = nuovo_giocaotre
			else:
				raise Exception('Tipologia mossa non valida')
			#al termine stampo lo schema
			schema_gioco.stampa_schema(lista_giocaotri=lista_giocatori)

	print('\n\nPARTITA FINITIA')
	#stringa_continuare = input('Si desidera cominciare una nuova partita? lasciare vuoto per si: ')

