#script che contiene la classe del giocatore con i relativi metodi

from __future__ import annotations
from typing import Any, List, Tuple, Union

#import numpy as np
import random
from moduli.EsitoSimulazioneMosse import EsitoSimulazioneMosse
from moduli.Mossa import Mossa, const_tipologia_mossa_muro, const_tipologia_mossa_spostamento

import moduli.Schema

const_numero_giocatore_disabilitato = 0
const_numero_giocatore_umano = 1
const_numero_giocatore_computer = 2

const_numero_muri_restanti_infinito = -1

const_non_prediligere_niente = 0
const_prediligi_muri = 1
const_prediligi_movimenti = 2

class Giocatore:
	posizione : Tuple[int, int]
	id_giocatore: int
	posizioni_vittoria: List[Tuple[int, int]]
	tipo_giocatore : int
	colore: str
	muri_restanti : int



	def __init__(self) -> None:
		#non fa niente per permettere le eventuali copie
		pass

	def __eq__(self, g2:Giocatore) -> bool:
		return g2.id_giocatore == self.id_giocatore

	def copy(self, nuova_posizione:Tuple[int, int], muri_restanti:int, flag_tutti_campi:bool) -> Giocatore:
		#metodo per copiare un giocatore, nel nuovo giocatore salvo solo id e cambio i riferimenti per giocatore successivo e precedente
		nuovo_giocatore = Giocatore()
		nuovo_giocatore.id_giocatore = self.id_giocatore
		nuovo_giocatore.posizione = nuova_posizione
		nuovo_giocatore.posizioni_vittoria = self.posizioni_vittoria
		nuovo_giocatore.muri_restanti = muri_restanti
		nuovo_giocatore.tipo_giocatore = self.tipo_giocatore
		#se ho messo il flag di tutti i campi faccio anche gli altri campi
		if flag_tutti_campi:
			nuovo_giocatore.colore = self.colore
		return nuovo_giocatore

	def popola_attributi(self, id_giocatore:int, posizione_partenza:Tuple[int, int], dim_v: int, dim_o : int, tipologia_giocatore: int, colore:str, muri_restanti:int) -> None:
		#qui non metto giocatore precedente e succesisvo
		#metodo per popolare gli attributi tutti
		self.id_giocatore = id_giocatore
		self.posizione = posizione_partenza
		self.tipo_giocatore = tipologia_giocatore
		self.colore = colore
		self.muri_restanti = muri_restanti
		if self.tipo_giocatore > 0:
			#adesso calcolo le caselle di vittoria
			(r_partenza, c_partenza) = posizione_partenza
			if r_partenza == 0:
				#parto dall'alto, le caselle che devo raggiungere sono quelle in fondo
				self.posizioni_vittoria = [ (dim_v-1, i) for i in range(dim_o) ]
			elif r_partenza == (dim_v -1):
				#parto dal basso, devo arrivare in alto
				self.posizioni_vittoria = [ (0, i) for i in range(dim_o) ]
			elif c_partenza == 0:
				#parto da sx, le caselle che devo raggiungere sono quelle in a dx
				self.posizioni_vittoria = [ (i, dim_o-1) for i in range(dim_v) ] 
			elif c_partenza == (dim_o -1):
				#parto da destra, devo arrivare a sx
				self.posizioni_vittoria = [ (i, 0) for i in range(dim_v) ] 
			else:
				raise Exception('Non identificato caselle arrivo')

	def popola_giocatore_disabilitato(self, id_giocatore:int, colore:str):
		#metodo per popolare gli attributi di un giocatore disabilitato
		self.id_giocatore = id_giocatore
		self.colore = colore
		self.tipo_giocatore = const_numero_giocatore_disabilitato

	def calcola_percorso_minimo(self, schema:moduli.Schema.Schema, lista_giocatori:List[Giocatore]) -> Union[List[Tuple[int, int]], None]:
		#metodo per calcolare i percorsi minimi per arrivare alla soluzione
		#se e un giocatore disabilitato ritorno subito None
#		if self.tipo_giocatore == const_numero_giocatore_disabilitato:
#			return None
		#per prima cosa verifico se sono arrivato alla soluzione
		if self.posizione in self.posizioni_vittoria:
			return []
		#adesso mi calcolo la lista delle celle gia analizzate
		lista_posizioni_gia_analizzate = [self.posizione]
		#mi calcolo la lista dei percorsi
		lista_percorsi = []
		#mi calcolo la lista delle posizioni degli altri giocatori
#		lista_posizioni_giocatori = [ i.posizione for i in lista_giocatori if ( (i.tipo_giocatore != const_numero_giocatore_disabilitato) and (i != self) and (i.posizione not in i.posizioni_vittoria) )]
		lista_posizioni_giocatori = [ i.posizione for i in lista_giocatori if ((i.tipo_giocatore != const_numero_giocatore_disabilitato) and (i != self) and (i.posizione not in i.posizioni_vittoria) )]
		#mi calcolo la lista delle posizioni da analizzare per la prima analisi ed eventualmente saltare caselle
		lista_caselle_adiacenti = [ i for i in schema.calcola_caselle_accettabili_successive(casella=self.posizione) ]
		#itero fino a quando la lista e una lista vuota
		while len(lista_caselle_adiacenti) > 0:
			#mi recupero la prima casella
			casella_in_analisi = lista_caselle_adiacenti.pop(0)
			#per prima cosa verifico se sono gia arrivato
			if casella_in_analisi in self.posizioni_vittoria:
				#ho trovato subito un percorso accettabile
				return [casella_in_analisi]
			#altrimenti la aggiungo a quelle analizzate
			lista_posizioni_gia_analizzate.append(casella_in_analisi)
			#adesso verifico se devo saltarla o meno
			if casella_in_analisi in lista_posizioni_giocatori:
				#significa che devo saltarla
				#mi calcolo la lista delle celle da analizzare
				prossime_celle_da_analizzare = [ i for i in schema.calcola_caselle_accettabili_successive(casella=casella_in_analisi) if i not in ( lista_caselle_adiacenti + lista_posizioni_gia_analizzate ) ]
				lista_caselle_adiacenti.extend(prossime_celle_da_analizzare)
			else:
				#posso aggiungere un percorso
				lista_percorsi.append( [ casella_in_analisi ] )
		#a questo punto ho una lista di percorsi, itero fino a quando la lista dei percori e vuota
		while len(lista_percorsi) > 0:
			percorso_in_analisi = lista_percorsi.pop(0)
			#mi calcolo la testa
			testa_percorso = percorso_in_analisi[-1]
			#calcolo tutte le caselle successive alla testa
			for casella_successiva in schema.calcola_caselle_accettabili_successive(casella=testa_percorso):
				#per prima cosa verifico se con questa casella arrivo alla fine
				if casella_successiva in self.posizioni_vittoria:
					#ritorno il percorso trovato
					return (percorso_in_analisi + [casella_successiva])
				#altrimenti verifico se non e gia stata analizzata
				if casella_successiva not in lista_posizioni_gia_analizzate:
					#la aggiungo subito
					lista_posizioni_gia_analizzate.append(casella_successiva)
					#adesso calcolo il nuovo percorso e lo aggiungo alla lista
					lista_percorsi.append(percorso_in_analisi + [casella_successiva])
		#al termine del while se non ha trovato percorsi ritorno None ad indicare che non e stato possibile trovare un percorso 
		return None

	def calcola_mosse_disponibili(self, schema:moduli.Schema.Schema, lista_giocatori:List[Giocatore]) -> List[Mossa]:
		#metodo per calcolare le mosse disponibili eventualmente saltando le caselle degli altri giocatori
		#se e un giocatore disabilitato ritorno subito None
#		if self.tipo_giocatore == const_numero_giocatore_disabilitato:
#			return []
		#per prima cosa verifico se sono arrivato alla soluzione
		if self.posizione in self.posizioni_vittoria:
			return []
		#adesso mi calcolo la lista delle celle gia analizzate
		lista_posizioni_gia_analizzate = [self.posizione]
		#mi calcolo la lista dei percorsi
		mosse_disponibili = []
		#mi calcolo la lista delle posizioni degli altri giocatori
#		lista_posizioni_giocatori = [ i.posizione for i in lista_giocatori if ( (i.tipo_giocatore!= const_numero_giocatore_disabilitato) and (i != self) and (i.posizione not in i.posizioni_vittoria) )]
		lista_posizioni_giocatori = [ i.posizione for i in lista_giocatori if ((i.tipo_giocatore != const_numero_giocatore_disabilitato) and (i != self) and (i.posizione not in i.posizioni_vittoria) )]
		#mi calcolo la lista delle posizioni da analizzare per la prima analisi ed eventualmente saltare caselle
		lista_caselle_adiacenti = [ i for i in schema.calcola_caselle_accettabili_successive(casella=self.posizione) ]
		#itero fino a quando la lista e una lista vuota
		while len(lista_caselle_adiacenti) > 0:
			#mi recupero la prima casella
			casella_in_analisi = lista_caselle_adiacenti.pop(0)
			#per prima cosa verifico se sono gia arrivato
			if casella_in_analisi in self.posizioni_vittoria:
				#ho trovato subito un percorso accettabile che porterebbe a una soluzione
				mosse_disponibili.append( Mossa(spostamento=(self.posizione, casella_in_analisi) ) )
			else:
				#altrimenti la aggiungo a quelle analizzate
				lista_posizioni_gia_analizzate.append(casella_in_analisi)
				#adesso verifico se devo saltarla o meno
				if casella_in_analisi in lista_posizioni_giocatori:
					#significa che devo saltarla
					#mi calcolo la lista delle celle da analizzare
					prossime_celle_da_analizzare = [ i for i in schema.calcola_caselle_accettabili_successive(casella=casella_in_analisi) if i not in ( lista_caselle_adiacenti + lista_posizioni_gia_analizzate ) ]
					lista_caselle_adiacenti.extend(prossime_celle_da_analizzare)
				else:
					#posso aggiungere la mossa
					mosse_disponibili.append( Mossa(spostamento=(self.posizione, casella_in_analisi) ) )
		#al termine del ciclo posso ritornare la lista delle mosse calcolate
		return mosse_disponibili

	def calcola_mossa_migliore_min_max(self, 
										schema_partenza:moduli.Schema.Schema, 
										lista_giocatori:List[Giocatore], 
										profondita :int, 
										flag_controlla_sempre_muri:bool, #flag che indica se controllare sempre i possibili muri
										modalita_prediligi : int, #numero che indica se prediligere i muri o le mosse spostamento, applicabile solo al primo caso
										flag_inverso:bool #fflag che indica se bisogna cercare di non arrivare mai alla destinazione (true), oppure se e il gioco normale(False)
									) -> EsitoSimulazioneMosse:
		#metodo per calcolare la migliore mossa da eseguire seguendo l'algoritmo min/max
		#per prima cosa calcolo il successivo giocatore
		giocatore_successivo = lista_giocatori[ (self.id_giocatore+1)%len(lista_giocatori) ]
		#verifico se sono arrivato alla fine della profondita
		if profondita <= 0:
			#mi creo una mossa ad hoc
			nuovo_esito = EsitoSimulazioneMosse()
			nuovo_esito.calcola_lunghezze_percorsi(schema=schema_partenza, lista_giocatori=lista_giocatori)
			return nuovo_esito
		#verifico se eun giocatore disabilitato
		if self.tipo_giocatore == const_numero_giocatore_disabilitato:
			#vadi direttametne al giocatore successivo
			return giocatore_successivo.calcola_mossa_migliore_min_max(schema_partenza=schema_partenza,
																		lista_giocatori=lista_giocatori,
																		profondita= profondita,
																		flag_controlla_sempre_muri=flag_controlla_sempre_muri,
																		modalita_prediligi=const_non_prediligere_niente,
																		flag_inverso=flag_inverso)
		#verifico subito se il giocatore attuale ha gia vinto
		if self.posizione in self.posizioni_vittoria:
			#ritorno lo stesso ritornato dal successivo ma ad una profondita minore per far terminare altrimenti rischia ciclo infinito in prossimita della vittoria
			return giocatore_successivo.calcola_mossa_migliore_min_max(schema_partenza=schema_partenza,
																		lista_giocatori=lista_giocatori,
																		profondita= profondita-1,
																		flag_controlla_sempre_muri=flag_controlla_sempre_muri,
																		modalita_prediligi=const_non_prediligere_niente,
																		flag_inverso=flag_inverso)
		#altrimenti e un giocatore ancora in gioco, mi calcolo le sue possibili mosse
		#mi calcolo i giocatori ancora in gioco
#		giocatori_in_gioco = [ i for i in lista_giocatori if ( (i.tipo_giocatore != const_numero_giocatore_disabilitato) and (i != self) and (i.posizione not in i.posizioni_vittoria) ) ]
		giocatori_in_gioco = [ i for i in lista_giocatori if ((i.tipo_giocatore != const_numero_giocatore_disabilitato) and (i != self) and (i.posizione not in i.posizioni_vittoria) ) ]
		lista_id_altri_giocatori = [ i.id_giocatore for i in giocatori_in_gioco ]
		#se non ci sono giocatori posso ritornare senza fare ulteriori prove
		if len(giocatori_in_gioco) == 0:
			nuovo_esito = EsitoSimulazioneMosse()
			nuovo_esito.calcola_lunghezze_percorsi(schema=schema_partenza, lista_giocatori=lista_giocatori)
			return nuovo_esito
		#ho trovato dei giocatori, provo a calcolarmi tutte le mosse che potrebbe fare 
		lista_esiti : List[EsitoSimulazioneMosse] = []
		#adesso eseguo tutte le mosse
		for mossa in self.calcola_mosse_disponibili(schema=schema_partenza, lista_giocatori=lista_giocatori):
			#eseguo la rispettiva mossa e vado a ricorsione
			nuovo_giocatore_mossa_eseguita = self.copy(nuova_posizione=mossa.spostamento[1], muri_restanti=self.muri_restanti, flag_tutti_campi=False)
			#mi calcolo la nuova lista dei giocatori
			nuova_lista_giocatori = lista_giocatori.copy()
			nuova_lista_giocatori[self.id_giocatore] = nuovo_giocatore_mossa_eseguita
			#mi calcolo l'esito della ricorsione
			esito_ricorsione = giocatore_successivo.calcola_mossa_migliore_min_max(schema_partenza=schema_partenza, #non faccio modifiche allo schema di partenza
																				lista_giocatori=nuova_lista_giocatori,
																				profondita=profondita-1,
																				flag_controlla_sempre_muri=flag_controlla_sempre_muri,
																				modalita_prediligi=const_non_prediligere_niente,
																				flag_inverso=flag_inverso)
			#adesso ci metto la mossa dello spostamento
			esito_ricorsione.spostamento = mossa.spostamento
			esito_ricorsione.tipologia_mossa = const_tipologia_mossa_spostamento
			#lo inserisco nella lista
			lista_esiti.append(esito_ricorsione)
		#se non voglio analizzare sempre i muri valuto se tutte le mosse disponibili non soddisfano
		flag_analisi_muri_necessaria_percorsi = False #flag che indica se e necessario analizzare i muri perche le mosse non soddisfano
		if (self.muri_restanti > 0) or (self.muri_restanti == const_numero_muri_restanti_infinito):
			#solo se ho la possibilita di inserire ancora muri posso analizzare questa cosa
			if not(flag_controlla_sempre_muri):
				#mi calcolo le minime lunghezze delle mosse
				if ( not(flag_inverso) and (min( [ e.calcola_massima_differenza_percorsi(id_giocatore_analisi=self.id_giocatore, lista_id_altri_giocatori=lista_id_altri_giocatori, flag_inverso=flag_inverso) for e in lista_esiti ] ) >= 0) ) or  ( flag_inverso and (max( [ e.calcola_massima_differenza_percorsi(id_giocatore_analisi=self.id_giocatore, lista_id_altri_giocatori=lista_id_altri_giocatori, flag_inverso=flag_inverso) for e in lista_esiti ] ) <= 0) ):
					#se la minima differenza e comunque positiva significa che c'è qualcun altro che rischia di fare un percorso piu corto
					flag_analisi_muri_necessaria_percorsi = True
			#adesso analizzo eventualmetne l'inserimento di muri
			if (flag_controlla_sempre_muri or flag_analisi_muri_necessaria_percorsi):
				#mi calcolo intanto i percorsi minimi per il giocatore in analisi 
				percorso_minimo_giocatore_attuale = self.calcola_percorso_minimo(schema=schema_partenza, lista_giocatori=lista_giocatori) #questo viene lasciato anche con flag inverso 
				assert percorso_minimo_giocatore_attuale is not None
				lunghezza_percorso_giocatore_in_analisi = len(percorso_minimo_giocatore_attuale)
				lista_percorsi_minimi  = [ g.calcola_percorso_minimo(schema=schema_partenza, lista_giocatori=lista_giocatori) for g in giocatori_in_gioco ]
				lista_percorsi_minimi = [ ( [] if i is None else i ) for i in lista_percorsi_minimi ]
				#assert all([ (i is not None) for i in lista_percorsi_minimi  ])
				lunghezze_lista_percorsi_minimi = [ len(i) for i in lista_percorsi_minimi ]
				#adesso devo calcolare una lista di celle cdi possibili muri: parto considerando solo quelli comuni a tutti i giocatori che hanno un percorso minore rispetto a quello attuale
				(dim_v, dim_o) = schema_partenza.schema_attuale.shape
				insieme_celle_possibili_muri = set( [ (r, c) for r in range(dim_v) for c in range(dim_o) ] )
				if flag_inverso:
					#devo cercare di allungarmi il percorso il piu possibile oppure impedire che gli altri riescano ad allungare il priorio percorso
					#i possibili muri sono il mio percorso oppure le caselle dove si possono muovere gli altri
					insieme_celle_possibili_muri = set(percorso_minimo_giocatore_attuale)
					#ci aggiungo le posizioni dei giocatori che hanno un percorso più lungo
					giocatori_con_percorso_piu_lungo = [ giocatori_in_gioco[i] for i in range(len(giocatori_in_gioco)) if lunghezze_lista_percorsi_minimi[i] >= lunghezza_percorso_giocatore_in_analisi ]
					for g in giocatori_con_percorso_piu_lungo:
						for mossa in g.calcola_mosse_disponibili(schema=schema_partenza, lista_giocatori=lista_giocatori):
							insieme_celle_possibili_muri.add(mossa.spostamento[1])
				else:
					#logica normale in cui cerco di allungare quello degli altri o accorciare il mio
					lista_percorsi_piu_corti = [ lista_percorsi_minimi[i] for i in range(len(lista_percorsi_minimi)) if lunghezze_lista_percorsi_minimi[i] <= lunghezza_percorso_giocatore_in_analisi ]
					if len(lista_percorsi_piu_corti) == 0:
						#se è zero significa che non hopercorsi più corti e prendo subito l'insieme del piu corto
						insieme_celle_possibili_muri = set( lista_percorsi_minimi[ lunghezze_lista_percorsi_minimi.index(min(lunghezze_lista_percorsi_minimi)) ] )
					else:
						#ci sono percorsi piu corti
						for percorso in lista_percorsi_piu_corti:
							assert percorso is not None
							insieme_celle_possibili_muri.intersection_update( set(percorso) )
						if len(insieme_celle_possibili_muri) == 0:
							#se zero significa che devo cercare le celle per i muri che compaiono in più percorsi, pur sapendo che non posso prenderli tutti
							diz_possibili_muri_numero_percorsi = dict()
							insieme_celle_distinte_percorsi = set()
							for percorso in lista_percorsi_piu_corti:
								insieme_celle_distinte_percorsi.update(set(percorso))
							for cella in insieme_celle_distinte_percorsi:
								diz_possibili_muri_numero_percorsi[cella] = sum( [ 1 for p in lista_percorsi_piu_corti if cella in p ] )
							#adesso devo semplicemente prendere quelli che compaiono in piu percorsi
							massima_comparizione = max( diz_possibili_muri_numero_percorsi.values() )
							insieme_celle_possibili_muri = set( [ k for k in diz_possibili_muri_numero_percorsi if diz_possibili_muri_numero_percorsi[k] == massima_comparizione ] )
					#a questo punto l'insieme delle celle possibili muri e popolato, provo ad inserire ciascun muro e vedere se i giocatori riescono comunque ad arrivare a destinazione
					if len(insieme_celle_possibili_muri) <= len(giocatori_in_gioco):
						insieme_celle_possibili_muri = set( lista_percorsi_minimi[ lunghezze_lista_percorsi_minimi.index(min(lunghezze_lista_percorsi_minimi)) ] )
				#mi creo le liste anche per verificare il giocatore attuale
				lista_giocatori_in_gioco_con_attuale = giocatori_in_gioco + [self]
				lista_percorsi_minimi_con_attuale = lista_percorsi_minimi + [ percorso_minimo_giocatore_attuale ]
				numero_giocatori_in_gioco_con_giocatore_attuale = len(lista_giocatori_in_gioco_con_attuale)
				lista_posizioni = [ g.posizione for g in lista_giocatori_in_gioco_con_attuale ]
				for possibile_muro in insieme_celle_possibili_muri:
					nuovo_schema = schema_partenza.inserisci_muro(posizione_muro=possibile_muro)
					if (possibile_muro in lista_posizioni) or (any( [ lista_giocatori_in_gioco_con_attuale[i].calcola_percorso_minimo(schema=nuovo_schema, lista_giocatori=lista_giocatori) is None  for i in range(numero_giocatori_in_gioco_con_giocatore_attuale) if possibile_muro in lista_percorsi_minimi_con_attuale[i] ] )): #lascio la condizione perche non puo intralciare anche gli altri giocatori anche se sto giocando con il flag inverso
						#se trova qualche giocatore che non puo completare il percorso, passa al muro successivo
						continue
					#se arrivo qui significa che tutti possono arrivare a un percorso, mi calcolo la ricorsione
					#mi calcolo anche la nuova lisat di giocatori se devo diminuire il muro del giocatore attuale
					if self.muri_restanti == const_numero_muri_restanti_infinito:
						#la lista e la stessa di prima
						nuova_lista_giocatori = lista_giocatori
					else:
						nuova_lista_giocatori = lista_giocatori.copy()
						nuovo_giocatore = self.copy(nuova_posizione=self.posizione, muri_restanti=self.muri_restanti-1, flag_tutti_campi=False)
						nuova_lista_giocatori[self.id_giocatore] = nuovo_giocatore
					esito_ricorsione = giocatore_successivo.calcola_mossa_migliore_min_max(schema_partenza=nuovo_schema, #non faccio modifiche allo schema di partenza
																					lista_giocatori=nuova_lista_giocatori,
																					profondita=profondita-1,
																					flag_controlla_sempre_muri=flag_controlla_sempre_muri,
																					modalita_prediligi=const_non_prediligere_niente,
																					flag_inverso=flag_inverso)
					#adesso ci metto la mossa dello spostamento
					esito_ricorsione.muro = possibile_muro
					esito_ricorsione.tipologia_mossa = const_tipologia_mossa_muro
					#lo inserisco nella lista
					lista_esiti.append(esito_ricorsione)
		#a questo punto ho la lista degli esiti popolata, mi calcolo quella che ottimizza il punteggio, quindi quella che ha una minima differenza
		lista_differenze = [e.calcola_massima_differenza_percorsi(id_giocatore_analisi=self.id_giocatore, lista_id_altri_giocatori=lista_id_altri_giocatori, flag_inverso=flag_inverso) for e in lista_esiti ]
		if flag_inverso:
			minima_differenza = max(lista_differenze)
		else:
			minima_differenza = min( lista_differenze )
		#faccio distinzione in base alla modalita
		if modalita_prediligi == const_non_prediligere_niente:
			lista_prioritaria = [ lista_esiti[i] for i in range(len(lista_esiti)) if lista_differenze[i] == minima_differenza ]
		elif modalita_prediligi == const_prediligi_muri:
			lista_prioritaria = [ lista_esiti[i] for i in range(len(lista_esiti)) if ((lista_differenze[i] == minima_differenza) and (lista_esiti[i].tipologia_mossa == const_tipologia_mossa_muro)) ]
			if len(lista_prioritaria) == 0:
				#se vuota prendo comunque la lista
				lista_prioritaria = [ lista_esiti[i] for i in range(len(lista_esiti)) if lista_differenze[i] == minima_differenza ]
		elif modalita_prediligi == const_prediligi_movimenti:
			lista_prioritaria = [ lista_esiti[i] for i in range(len(lista_esiti)) if ((lista_differenze[i] == minima_differenza) and (lista_esiti[i].tipologia_mossa == const_tipologia_mossa_spostamento)) ]
			if len(lista_prioritaria) == 0:
				#se vuota prendo comuqnue la lista classica
				lista_prioritaria = [ lista_esiti[i] for i in range(len(lista_esiti)) if lista_differenze[i] == minima_differenza ]
		else:
			raise Exception('Modalita privilegi non riconosciuta')
		#lista_esiti_minima_differenza = [ lista_esiti[i] for i in range(len(lista_esiti)) if lista_differenze[i] == minima_differenza ]
		return random.choice(lista_prioritaria)
		
		








