#script per contenere la main window del quoridor

import os, numpy as np, time
from typing import Dict, List, Tuple

from PyQt5 import QtCore, QtGui, QtWidgets
from moduli.Giocatore import Giocatore, const_numero_giocatore_disabilitato, const_numero_muri_restanti_infinito, const_prediligi_movimenti, const_prediligi_muri, const_non_prediligere_niente, const_numero_giocatore_computer, const_numero_giocatore_umano
from moduli.GraficaQuoridor import Ui_GraficaQuoridor
from moduli.Partita import Partita, const_diz_associazione_colori
from moduli.Schema import const_casella_muro
import moduli.ThreadGrafica as ThreadGrafica

const_colore_bordo_vuoto = 'darkGray'
const_colore_casella_vuota = 'white'
const_colore_selezione = 'lightGray'
const_colore_ultima_mossa = 'blue'
const_colore_ultimo_muro = 'gray'
const_colore_muro = 'black'
const_colore_mossa_successiva = 'yellow'

const_cartella_main = os.path.dirname(os.path.dirname( os.path.abspath(__file__) ))
const_stringa_file_immagine_pedina = os.path.join(const_cartella_main, 'immagini_pedine', 'pedina_{colore}.png' )

class MainWindowQuoridor(QtWidgets.QMainWindow):
	#estende la classe di qt

	grafica_ui : Ui_GraficaQuoridor
	partita_attuale : Partita
	#lista_celle_selezionate : List[List[Tuple[int, int]]] #non utilizzata perche dentro i giocatori
	giocatore_attuale : int #indice che indica quale giocatore deve muovere
	thread_attesa_mossa_pc : ThreadGrafica.ThreadGrafica
	dizionario_immagini:Dict[str, QtGui.QPixmap] #dizionario contenente le icone per evitare ogni volta di aprire un file, ho come chiavi i file
	lista_select_giocatori : List[QtWidgets.QComboBox]
	lista_ultime_mosse : List[List[Tuple[int, int]]] #lista delle ultime mosse dei giocatori in coordinate schema mostra, non schema effettivo
	lista_tempi_fine : List[float]
	

	def __init__(self) -> None:
		super().__init__()

		ui = Ui_GraficaQuoridor()
		ui.setupUi(self)

		self.grafica_ui = ui
		self.grafica_ui.label_giocatore_attuale.setText("")
		self.dizionario_immagini = {}
		self.lista_select_giocatori = [ self.grafica_ui.comboBox_giocatore_1,self.grafica_ui.comboBox_giocatore_2, self.grafica_ui.comboBox_giocatore_3, self.grafica_ui.comboBox_giocatore_4 ]
		self.lista_ultime_mosse = []
		self.lista_tempi_fine = []

	############################################################################################################################################################
	#funzioni di controllo della grafica
	def verifica_almeno_un_giocatore(self, stringa_cambiata):
		#metodo per verificare se esiste almeno un giocatore tra quelli selezionati, altrimenti mette l'umano nel primo giocatore
		if stringa_cambiata == self.grafica_ui.comboBox_giocatore_1.itemText(0):
			#ho disabilitato il giocatore, vedo quanti solo quelli disabilitati
			if len( [ 1 for i in self.lista_select_giocatori if i.currentIndex()!=0 ] ) == 0:
				#non ci sono giocatori selezionati, seleziono il primo
				self.grafica_ui.comboBox_giocatore_1.setCurrentIndex(1)

	def mostra_lista_parametri(self):
		#metodo per popolare i parametri della partita
		#per prima cosa svuoto i parametri attuali
		#self.grafica_ui.tableWidget_parametri.setRowCount(0)
		#mi recuper le dimensioni
		(dim_v, dim_o) = self.partita_attuale.schema_attuale.schema_attuale.shape
		#metto intanto i parametri per le dimensioni
		self.grafica_ui.tableWidget_parametri.setItem(0, 0, QtWidgets.QTableWidgetItem(str(dim_v)))
		self.grafica_ui.tableWidget_parametri.setItem(1, 0, QtWidgets.QTableWidgetItem(str(dim_o)))
		#calcolo la stringa per analizzare i muri o meno
		stringa_analizza_muri = 'Si' if self.partita_attuale.flag_analizza_muri else 'No'
		self.grafica_ui.tableWidget_parametri.setItem(2, 0, QtWidgets.QTableWidgetItem(stringa_analizza_muri))
		#creo la stringa per il numero muri per giocatore
		stringa_numero_muri = 'Infiniti' if (self.partita_attuale.numero_muri_iniziali == const_numero_muri_restanti_infinito) else str(self.partita_attuale.numero_muri_iniziali)
		self.grafica_ui.tableWidget_parametri.setItem(3, 0, QtWidgets.QTableWidgetItem(stringa_numero_muri))
		#adesso calcolo il tipo di priorita
		if self.partita_attuale.tipologia_priorita == const_non_prediligere_niente:
			stringa_priorita = 'Nessuna'
		elif self.partita_attuale.tipologia_priorita == const_prediligi_movimenti:
			stringa_priorita = 'Movimenti'
		elif self.partita_attuale.tipologia_priorita == const_prediligi_muri:
			stringa_priorita = 'Muri'
		else:
			raise Exception('Priorità non riconosciuta')
		self.grafica_ui.tableWidget_parametri.setItem(4, 0, QtWidgets.QTableWidgetItem(stringa_priorita))

	def mostra_lista_giocatori(self):
		#metodo per popolare la lista dei giocatori con la relativa classifica
		#per prima cosa svuoto la relativa lista
		self.grafica_ui.tableWidget_classifica.setRowCount(0)
		#poi prendo prima i giocatori che hanno gia vinto e in successione quelli ancora in gioco
		posizione_attuale = 1
		#distinguo in base al flag inverso 
		if self.partita_attuale.flag_inverso:
			#devo invertirli
			#adesso prendo tutti gli altri in base alla lunghezza del percorso minimo
			for g_non_vincitore in sorted( [ i for i in self.partita_attuale.lista_giocatori if ((i.tipo_giocatore != const_numero_giocatore_disabilitato) and (self.lista_tempi_fine[i.id_giocatore] == 0)) ], key= lambda x: -len( x.calcola_percorso_minimo(schema=self.partita_attuale.schema_attuale, lista_giocatori=self.partita_attuale.lista_giocatori) ) ):
				#aggiungo una riga alla tabella
				self.grafica_ui.tableWidget_classifica.setRowCount( self.grafica_ui.tableWidget_classifica.rowCount() +1 )
				riga_giocatore = posizione_attuale -1
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 0, QtWidgets.QTableWidgetItem(str(posizione_attuale) + '*')) #posizione
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 1, QtWidgets.QTableWidgetItem(g_non_vincitore.colore + ( ' (C)' if g_non_vincitore.tipo_giocatore == const_numero_giocatore_computer else ' (U)' ) )) #colore giocatore
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 2, QtWidgets.QTableWidgetItem(str(len( g_non_vincitore.calcola_percorso_minimo(schema=self.partita_attuale.schema_attuale, lista_giocatori=self.partita_attuale.lista_giocatori))))) #mosse per vincere
				#calcolo la stringa del numero di muri restanti	
				stringa_muri = 'Infiniti' if g_non_vincitore.muri_restanti == const_numero_muri_restanti_infinito else str(g_non_vincitore.muri_restanti)
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 3, QtWidgets.QTableWidgetItem(stringa_muri)) #muri a disposizione
				#incremento la posizione
				posizione_attuale += 1
			for g_vincitore in sorted( [ i for i in self.partita_attuale.lista_giocatori if ((i.tipo_giocatore != const_numero_giocatore_disabilitato) and (self.lista_tempi_fine[i.id_giocatore] > 0)) ], key= lambda x: -self.lista_tempi_fine[x.id_giocatore] ):
				#aggiungo una riga alla tabella
				self.grafica_ui.tableWidget_classifica.setRowCount( self.grafica_ui.tableWidget_classifica.rowCount() +1 )
				riga_giocatore = posizione_attuale -1
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 0, QtWidgets.QTableWidgetItem(str(posizione_attuale))) #posizione
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 1, QtWidgets.QTableWidgetItem(g_vincitore.colore + ( ' (C)' if g_vincitore.tipo_giocatore == const_numero_giocatore_computer else ' (U)' ) )) #colore giocatore
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 2, QtWidgets.QTableWidgetItem('0')) #mosse per vincere
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 3, QtWidgets.QTableWidgetItem('-')) #muri a disposizione
				#incremento la posizione
				posizione_attuale += 1
		else:
			for g_vincitore in sorted( [ i for i in self.partita_attuale.lista_giocatori if ((i.tipo_giocatore != const_numero_giocatore_disabilitato) and (self.lista_tempi_fine[i.id_giocatore] > 0)) ], key= lambda x: self.lista_tempi_fine[x.id_giocatore] ):
				#aggiungo una riga alla tabella
				self.grafica_ui.tableWidget_classifica.setRowCount( self.grafica_ui.tableWidget_classifica.rowCount() +1 )
				riga_giocatore = posizione_attuale -1
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 0, QtWidgets.QTableWidgetItem(str(posizione_attuale))) #posizione
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 1, QtWidgets.QTableWidgetItem(g_vincitore.colore + ( ' (C)' if g_vincitore.tipo_giocatore == const_numero_giocatore_computer else ' (U)' ) )) #colore giocatore
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 2, QtWidgets.QTableWidgetItem('0')) #mosse per vincere
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 3, QtWidgets.QTableWidgetItem('-')) #muri a disposizione
				#incremento la posizione
				posizione_attuale += 1
			#adesso prendo tutti gli altri in base alla lunghezza del percorso minimo
			for g_non_vincitore in sorted( [ i for i in self.partita_attuale.lista_giocatori if ((i.tipo_giocatore != const_numero_giocatore_disabilitato) and (self.lista_tempi_fine[i.id_giocatore] == 0)) ], key= lambda x: len( x.calcola_percorso_minimo(schema=self.partita_attuale.schema_attuale, lista_giocatori=self.partita_attuale.lista_giocatori) ) ):
				#aggiungo una riga alla tabella
				self.grafica_ui.tableWidget_classifica.setRowCount( self.grafica_ui.tableWidget_classifica.rowCount() +1 )
				riga_giocatore = posizione_attuale -1
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 0, QtWidgets.QTableWidgetItem(str(posizione_attuale) + '*')) #posizione
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 1, QtWidgets.QTableWidgetItem(g_non_vincitore.colore + ( ' (C)' if g_non_vincitore.tipo_giocatore == const_numero_giocatore_computer else ' (U)' ) )) #colore giocatore
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 2, QtWidgets.QTableWidgetItem(str(len( g_non_vincitore.calcola_percorso_minimo(schema=self.partita_attuale.schema_attuale, lista_giocatori=self.partita_attuale.lista_giocatori))))) #mosse per vincere
				#calcolo la stringa del numero di muri restanti	
				stringa_muri = 'Infiniti' if g_non_vincitore.muri_restanti == const_numero_muri_restanti_infinito else str(g_non_vincitore.muri_restanti)
				self.grafica_ui.tableWidget_classifica.setItem(riga_giocatore, 3, QtWidgets.QTableWidgetItem(stringa_muri)) #muri a disposizione
				#incremento la posizione
				posizione_attuale += 1

	####################################################################################################################################################################################
	#funzioni richieste dalla grafica
	def ridimensiona_caselle(self, dim:int):
		#metodo per ridimensionare le caselle della grafica
		#metto le dimensioni delle colonne
		#for r in range(self.grafica_ui.tableWidget_schema_gioco.rowCount()):
		#	self.grafica_ui.tableWidget_schema_gioco.setRowHeight(r, dim)
		#for c in range(self.grafica_ui.tableWidget_schema_gioco.columnCount()):
		#	self.grafica_ui.tableWidget_schema_gioco.setColumnWidth(c, dim)
		self.aggiorna_schema_mostrato()

	def inizia_nuova_partita(self):
		#metodo per iniziare una nuova partita
		self.showMaximized()
		#recupero i valori da input
		dim_v = self.grafica_ui.spinBox_dim_v.value()
		dim_o = self.grafica_ui.spinBox_dim_o.value()
		flag_analizza_muri = self.grafica_ui.checkBox_analizza_muri.isChecked()
		tipo_priorita = self.grafica_ui.comboBox_prediligi.currentIndex()
		numero_muri_per_giocatore = self.grafica_ui.spinBox_muri_per_giocatore.value()
		(tipo_g1, tipo_g2, tipo_g3, tipo_g4) = [ i.currentIndex() for i in self.lista_select_giocatori ]
		flag_inverso = self.grafica_ui.checkBox_flagInverso.isChecked()

		#creo la nuova partita
		nuova_partita = Partita(dim_v=dim_v,
								dim_o=dim_o,
								flag_analizza_muri=flag_analizza_muri,
								numero_muri_per_giocatore=numero_muri_per_giocatore,
								tipo_priorita=tipo_priorita,
								tipologia_giocatore_1=tipo_g1,
								tipologia_giocatore_2=tipo_g2,
								tipologia_giocatore_3=tipo_g3,
								tipologia_giocatore_4=tipo_g4,
								flag_inverso=flag_inverso)
		#popolo l'attributo
		self.partita_attuale = nuova_partita
		#popolo il giocatore attuale come il minimo in gioco
		self.giocatore_attuale = max( [ i.id_giocatore for i in self.partita_attuale.lista_giocatori if i.tipo_giocatore!= const_numero_giocatore_disabilitato ] )
		self.dizionario_immagini.clear()
		#mi popolo la lista delle ultime mosse
		self.lista_ultime_mosse = [ [] for i in self.partita_attuale.lista_giocatori ]
		#popolo la lista dei tempi di conclusione
		self.lista_tempi_fine = [0 for i in self.partita_attuale.lista_giocatori]
		#vado a farmi la tabella di dimensioni prestabilite
		self.grafica_ui.tableWidget_schema_gioco.setColumnCount(dim_o+2)
		self.grafica_ui.tableWidget_schema_gioco.setRowCount(dim_v+2)
		#mostro la lista dei parametri
		self.mostra_lista_parametri()
		#chiudo eventuale thread di mosse che gira
		if hasattr(self, "thread_attesa_mossa_pc"):
			self.thread_attesa_mossa_pc.killTimer(0)
			del self.thread_attesa_mossa_pc
		#mostro la lista dei giocatori
		#self.mostra_lista_giocatori()
		#aggiorno subito lo schema di gioco per vedere se funziona
		#self.aggiorna_schema_mostrato()
		#mi sposto nella terza scheda
		self.grafica_ui.toolBox.setCurrentIndex(2)
		#massimizzo la finestra
		
		#metto una mossa nel primo giocatore
		#self.lista_ultime_mosse[self.giocatore_attuale].append( (self.partita_attuale.lista_giocatori[self.giocatore_attuale].posizione[0]+1,self.partita_attuale.lista_giocatori[self.giocatore_attuale].posizione[1]+1) )
		#self.lista_ultime_mosse[1].extend( [ (1,1), (1,2) ] )
		#self.lista_ultime_mosse[3].extend( [ (4,2) ] )
		#avvio il turno
		self.termina_turno()

	def gestisci_click_casella(self, r_grafica:int, c_grafica:int):
		#metodo per gestire il click di una casella
		#print('gestisci click casella')
		#primo controllo: sia dentro lo schema di gioco e non nel bordo
		if (0 < r_grafica < (self.grafica_ui.tableWidget_schema_gioco.rowCount() -1)) and ( 0 < c_grafica < (self.grafica_ui.tableWidget_schema_gioco.columnCount() -1) ):
			#recupero il giocatore attuale e intervengo solo se è un umano
			giocatore_attuale = self.partita_attuale.lista_giocatori[self.giocatore_attuale]
			if giocatore_attuale.tipo_giocatore == const_numero_giocatore_umano:
				#se il giocatore non ha selezionato nessuna casella, allora seleziono quella casella
				if len(self.lista_ultime_mosse[giocatore_attuale.id_giocatore]) == 0:
					#posso aggiungerla solo se: e la posizione del giocatore, e una casella libera e il giocatore ha ancora muri a disposizione
					if (r_grafica-1, c_grafica-1) == giocatore_attuale.posizione:
						self.lista_ultime_mosse[giocatore_attuale.id_giocatore].append( (r_grafica, c_grafica) )
					elif (self.partita_attuale.numero_muri_iniziali == const_numero_muri_restanti_infinito) or (giocatore_attuale.muri_restanti >0):
						#controllo se il giocatore ha ancora muri
						#adesso controllo che la casella selezionata sia vuota e non sia la posizione di qualche altro giocatore
						if (r_grafica-1, c_grafica-1) not in ( 
										list(zip( *np.where(self.partita_attuale.schema_attuale.schema_attuale == const_casella_muro) )) #lista dei muri
										+
										[ g.posizione for g in self.partita_attuale.lista_giocatori if ( (g.tipo_giocatore != const_numero_giocatore_disabilitato) and (g.posizione not in g.posizioni_vittoria) and (g != giocatore_attuale) ) ] #lista delle posizioni degli altri giocatori
								):
							#adesso devo vedere se mettendo un muro in quella posizione tutti i giocatori possono arrivare alla soluzione
							nuovo_schema = self.partita_attuale.schema_attuale.inserisci_muro(posizione_muro=(r_grafica-1, c_grafica-1))
							if all( [g.calcola_percorso_minimo(schema=nuovo_schema, lista_giocatori=self.partita_attuale.lista_giocatori) is not None for g in self.partita_attuale.lista_giocatori if ( (g.tipo_giocatore != const_numero_giocatore_disabilitato) and (g.posizione not in g.posizioni_vittoria) ) ] ):
								#se tutti i giocatori ancora in gioco potrebbero raggiungere la soluzione allora il muro e possibile
								self.lista_ultime_mosse[giocatore_attuale.id_giocatore].append( (r_grafica, c_grafica) )
				else:
					#il giocatore ha gia selezionato una mossa
					#se la coordinata presente nella lista e quella della posizione, significa che devo vedere se la casella selezionata appartiene a quelle adiacenti
					(r_ultima, c_ultima) = self.lista_ultime_mosse[giocatore_attuale.id_giocatore][0]
					r_coord_schema = r_ultima -1
					c_coord_schema = c_ultima -1
					if (( r_coord_schema, c_coord_schema ) == giocatore_attuale.posizione): 
						#verifico se ho cliccato nuovamente sulla cella di partenza per selezionarla
						if (r_grafica == r_ultima) and (c_grafica == c_ultima):
							#ho cliccato sulla stessa casella per deselezionarla
							#svuoto la lista delle mosse
							self.lista_ultime_mosse[giocatore_attuale.id_giocatore].clear()
						elif ( (r_grafica-1, c_grafica-1) in [i.spostamento[1] for i in giocatore_attuale.calcola_mosse_disponibili(schema=self.partita_attuale.schema_attuale, lista_giocatori=self.partita_attuale.lista_giocatori)] ):
							#allora ho selezionato una mossa valida, la compio
							self.partita_attuale.lista_giocatori = self.partita_attuale.lista_giocatori.copy()
							nuovo_giocaotre = giocatore_attuale.copy(nuova_posizione=(r_grafica-1, c_grafica-1), muri_restanti=giocatore_attuale.muri_restanti, flag_tutti_campi=True)
							self.partita_attuale.lista_giocatori[giocatore_attuale.id_giocatore] = nuovo_giocaotre
							#adesso termino il turno
							self.lista_ultime_mosse[giocatore_attuale.id_giocatore].append( (r_grafica, c_grafica) )
							self.termina_turno()
					elif (r_grafica == r_ultima) and (c_grafica == c_ultima):
						#ho prima selezionato un muro e poi ho ricliccato per confermarlo
						self.partita_attuale.schema_attuale = self.partita_attuale.schema_attuale.inserisci_muro(posizione_muro=(r_coord_schema, c_coord_schema) )
						if self.partita_attuale.numero_muri_iniziali != const_numero_muri_restanti_infinito:
							#e la lista aggiornata dei giocatori con un muro in meno
							self.partita_attuale.lista_giocatori = self.partita_attuale.lista_giocatori.copy()
							nuovo_giocaotre = giocatore_attuale.copy(nuova_posizione=giocatore_attuale.posizione, muri_restanti=giocatore_attuale.muri_restanti-1, flag_tutti_campi=True)
							self.partita_attuale.lista_giocatori[giocatore_attuale.id_giocatore] = nuovo_giocaotre
						#adesso termino il turno
						self.termina_turno()
					else:
						#ho cliccato da un'altra parte, svuoto la lista e richiamo la stessa procedura a ricorsione per gestire il caso pulito
						self.lista_ultime_mosse[giocatore_attuale.id_giocatore].clear()
						self.gestisci_click_casella(r_grafica=r_grafica, c_grafica=c_grafica)
				#al termine di tutto aggiorno la grafica
				self.aggiorna_schema_mostrato()


	############################################################################################################################################################################################
	#funzione per creare lo schema
	def aggiorna_schema_mostrato(self):
		#per prima cosa valuto se ho una partita in gioco
		if hasattr(self, 'partita_attuale'):
			#se ce l'ha allora svuoto lo schema di gioco
			(dim_v, dim_o) = self.partita_attuale.schema_attuale.schema_attuale.shape
			#adesso vado a mettere il bordo nero su tutta la cornice
			dim_v_schema_grafica = self.grafica_ui.tableWidget_schema_gioco.rowCount()
			dim_o_schema_grafica = self.grafica_ui.tableWidget_schema_gioco.columnCount()
			#vado solo a eliminarmi tutti gli elementi
			for r in range(dim_v+2):
				for c in range(dim_o+2):
					item_casella = self.grafica_ui.tableWidget_schema_gioco.item(r, c) #devo mettere il +1 perche la dimensione dello schema di gioco avra una cornice esterna rispetto allo schema originale
					self.grafica_ui.tableWidget_schema_gioco.removeCellWidget(r, c)
					del item_casella
			#recupero la dimensione dei quadrati
			dimensione_quadrati = self.grafica_ui.spinBox_dimensione_caselle.value()
			#aggiorno le dimensioni della griglia
			self.grafica_ui.tableWidget_schema_gioco.horizontalHeader().setDefaultSectionSize(dimensione_quadrati)
			self.grafica_ui.tableWidget_schema_gioco.horizontalHeader().setMinimumSectionSize(dimensione_quadrati)
			self.grafica_ui.tableWidget_schema_gioco.verticalHeader().setDefaultSectionSize(dimensione_quadrati)
			self.grafica_ui.tableWidget_schema_gioco.verticalHeader().setMinimumSectionSize(dimensione_quadrati)
			#adesso vado a mettere un elemento vuoto in ogni cella
			for r in range(dim_v_schema_grafica):
				for c in range(dim_o_schema_grafica):
					#scelgo il colore di sfondo
					if (r==0) or (r==dim_v_schema_grafica-1) or (c==0) or (c == dim_o_schema_grafica-1):
						#allora e una cella del bordo
						colore_sfondo = const_colore_bordo_vuoto
					else:
						#e una casella normale, ci metto il bianco
						colore_sfondo = const_colore_casella_vuota
					#metto un quadrato nero 
					nuovo_item = QtWidgets.QTableWidgetItem()
					#ci metto sempre il colore nero
					nuovo_item.setBackground(QtGui.QColor(colore_sfondo))
					#aggiungo l'elemento alla tabella
					self.grafica_ui.tableWidget_schema_gioco.setItem(r, c, nuovo_item)
			'''
			for r in range(dim_v_schema_grafica):
				#metto un quadrato nero 
				nuovo_item_bordo = QtWidgets.QTableWidgetItem()
				#ci metto sempre il colore nero
				nuovo_item_bordo.setBackground(QtGui.QColor(const_colore_bordo_vuoto))
				#aggiungo l'elemento alla tabella
				self.grafica_ui.tableWidget_schema_gioco.setItem(r, 0, nuovo_item_bordo)
				#metto un quadrato nero 
				nuovo_item_bordo = QtWidgets.QTableWidgetItem()
				#ci metto sempre il colore nero
				nuovo_item_bordo.setBackground(QtGui.QColor(const_colore_bordo_vuoto))
				self.grafica_ui.tableWidget_schema_gioco.setItem(r, dim_o_schema_grafica-1, nuovo_item_bordo)
			#faccio la stessa cosa per le colonne
			for c in range(dim_o_schema_grafica):
				#aggiungo l'elemento alla tabella
				#metto un quadrato nero 
				nuovo_item_bordo = QtWidgets.QTableWidgetItem()
				#ci metto sempre il colore nero
				nuovo_item_bordo.setBackground(QtGui.QColor(const_colore_bordo_vuoto))
				self.grafica_ui.tableWidget_schema_gioco.setItem(0, c, nuovo_item_bordo)
				#metto un quadrato nero 
				nuovo_item_bordo = QtWidgets.QTableWidgetItem()
				#ci metto sempre il colore nero
				nuovo_item_bordo.setBackground(QtGui.QColor(const_colore_bordo_vuoto))
				self.grafica_ui.tableWidget_schema_gioco.setItem(dim_v_schema_grafica-1, c, nuovo_item_bordo)
			'''
			#prima di mettere i giocatori metto i muri cosi dopo posso ricolorare le caselle per i giocatori in base ai loro ultimi movimenti 
			for (r_muro, c_muro) in zip( *np.where(self.partita_attuale.schema_attuale.schema_attuale == const_casella_muro) ):
				self.grafica_ui.tableWidget_schema_gioco.item(r_muro+1, c_muro+1).setBackground(QtGui.QColor(const_colore_muro))
			#adesso metto i colori dei giocatori non ancora arrivati e abilitati
			for g in (self.partita_attuale.lista_giocatori[self.giocatore_attuale+1:] + self.partita_attuale.lista_giocatori[:self.giocatore_attuale] + [self.partita_attuale.lista_giocatori[self.giocatore_attuale]]): #lascio il giocatore attuale come ultimo
				if (g.tipo_giocatore != const_numero_giocatore_disabilitato):# and (g.posizione not in g.posizioni_vittoria):
					#allora il giocatore e ancora in gioco
					#mi calcolo massimo e minimo delle sue posizioni di vittoria e stabilisco dove devo colorare
					(r_max, c_max) = max(g.posizioni_vittoria)
					(r_min, c_min) = min(g.posizioni_vittoria)
					#adesso devo vedere cosa e uguale
					if r_max == r_min:
						#l'utente deve arrivare in uno dei 2 bordi sopra o sotto
						#mi calcolo dove deve arrivare
						r_riferimento = 0 if (r_max == 0) else (dim_v_schema_grafica-1)
						for c in range(1, dim_o_schema_grafica-1):
							#prendo l'item e ne modifico il background
							item_tabella = self.grafica_ui.tableWidget_schema_gioco.item(r_riferimento, c)
							item_tabella.setBackground(QtGui.QColor(const_diz_associazione_colori[g.colore]))
					elif c_max == c_min:
						#il giocatore deve arrivare in uno dei 2 bordi laterali
						#mi calcolo dove deve arrivare
						c_riferimento = 0 if (c_max == 0) else (dim_o_schema_grafica-1)
						for r in range(1, dim_v_schema_grafica-1):
							#prendo l'item e ne modifico il background
							item_tabella = self.grafica_ui.tableWidget_schema_gioco.item(r, c_riferimento)
							item_tabella.setBackground(QtGui.QColor(const_diz_associazione_colori[g.colore]))
					else:
						raise Exception('caso non gestito di bordo colorato')
					#adesso vado ad inserire la pedine del giocatore
					#mi ricavo il nome del file della pedina del giocatore
					nome_file_pedina = const_stringa_file_immagine_pedina.format( colore=const_diz_associazione_colori[g.colore] )
					if self.dizionario_immagini.get(nome_file_pedina) is not None:
						pic = self.dizionario_immagini.get(nome_file_pedina)
					else:
						pic =QtGui.QPixmap(nome_file_pedina)
						#la aggiungo al dizionario
						self.dizionario_immagini[nome_file_pedina] = pic
					assert isinstance(pic, QtGui.QPixmap)
					immagine_scalata = pic.scaledToHeight(dimensione_quadrati)
					label = QtWidgets.QLabel()
					label.setPixmap(immagine_scalata)
					label.setScaledContents(True)
					(r,c) = g.posizione
					self.grafica_ui.tableWidget_schema_gioco.setCellWidget(r+1, c+1, label)
					#adesso inserisco l'ultima mossa del giocatore
					#se il giocatore attuale, signifca che il colore sara giallo
					if g.id_giocatore == self.giocatore_attuale:
						colore = const_colore_selezione
					elif len(self.lista_ultime_mosse[g.id_giocatore]) == 1:
						#significa che prima ha messo un muro
						colore = const_colore_ultimo_muro
					else:
						#significa che prima ha mosso
						colore = const_colore_ultima_mossa
					for (r_prec, c_prec) in self.lista_ultime_mosse[g.id_giocatore]:
						#cambio sempre il colore perche se dove c'era prima adesso c'e un muro vuol dire che e stato inserito da un giocatore dopo
						self.grafica_ui.tableWidget_schema_gioco.item(r_prec, c_prec).setBackground(QtGui.QColor(colore))
					#valuto se mettere anche i colori dei succeessivi 
					if (self.giocatore_attuale == g.id_giocatore) and (len(self.lista_ultime_mosse[g.id_giocatore]) >0):
						#mi calcolo l'ultima mossa segnata
						(r_ultima, c_ultima) = self.lista_ultime_mosse[g.id_giocatore][0]
						#se e nella sua posizione mostro dove puo avanzare, altrimenti coloro di giallo quella casella
						if (r_ultima-1, c_ultima-1) == g.posizione:
							#allora mostro le mosse successive possibile
							for next_mossa in g.calcola_mosse_disponibili(schema=self.partita_attuale.schema_attuale, lista_giocatori=self.partita_attuale.lista_giocatori):
								(r_next, c_next) = next_mossa.spostamento[-1]
								self.grafica_ui.tableWidget_schema_gioco.item(r_next+1, c_next+1).setBackground(QtGui.QColor(const_colore_mossa_successiva))
						else:
							#ricoloro quella come successiva mossa
							self.grafica_ui.tableWidget_schema_gioco.item(r_ultima, c_ultima).setBackground(QtGui.QColor(const_colore_mossa_successiva))

	############################################################################################################################################################################################################
	#metodo per gestire la terminazione del turno
	def termina_turno(self):
		#metodo per terminare il turno di un giocatore, controllare se ha vinto ed eventualmetne mostrare messaggi all'utente e cominciare il turno successivo
		
		#per prima cosa controllo se il giocatore e arrivato a destinazione
		giocatore_attuale = self.partita_attuale.lista_giocatori[self.giocatore_attuale]
		if ( giocatore_attuale.tipo_giocatore != const_numero_giocatore_disabilitato ) and ( giocatore_attuale.posizione in giocatore_attuale.posizioni_vittoria ):
			#il giocatore ha appena vinto, aggiorno la lista dei tempi di vincita
			self.lista_tempi_fine[self.giocatore_attuale] = time.time()
			#mostro un alert all'utente dicendo che ha vinto
			msg = QtWidgets.QMessageBox()
			msg.setIcon(QtWidgets.QMessageBox.Information)
			msg.setText(f"Il giocatore {giocatore_attuale.colore} ha raggiunto la destinazione")
			msg.setWindowTitle("Giocatore arrivato")
			msg.exec_()
			#adesso vado a controllare gli altri giocatori
			lista_giocatori_in_gioco = [g for g in self.partita_attuale.lista_giocatori if ( (g.tipo_giocatore != const_numero_giocatore_disabilitato) and (g.posizione not in g.posizioni_vittoria) )] 
			if len(lista_giocatori_in_gioco) == 0:
				#non dovrebbe mai succedere sollevo eccezione
				raise Exception('Nessun giocatore in gioco')
			elif len(lista_giocatori_in_gioco) == 1:
				#e rimasto un solo giocatore, dichiaro la partita finita e dico che il giocatore restante ha perso
				giocatore_perdente = lista_giocatori_in_gioco.pop()
				self.partita_attuale.is_partita_finita = True
				#metto il tempo attuale
				self.lista_tempi_fine[giocatore_perdente.id_giocatore] = time.time()
				msg = QtWidgets.QMessageBox()
				msg.setIcon(QtWidgets.QMessageBox.Information)
				msg.setText(f"Il giocatore {giocatore_perdente.colore} ha perso in quanto ultimo rimasto")
				msg.setWindowTitle("Partita terminata")
				msg.exec_()
		#adesso verifico se la partita non e gia terminata, in caso contrario, avanzo di giocatore
		if not(self.partita_attuale.is_partita_finita):
			#cerco il primo giocatore non disabilitato 
			self.giocatore_attuale += 1
			self.giocatore_attuale %= len(self.partita_attuale.lista_giocatori)
			giocatore_successivo = self.partita_attuale.lista_giocatori[self.giocatore_attuale]
			while (giocatore_successivo.tipo_giocatore == const_numero_giocatore_disabilitato) or ( giocatore_successivo.posizione in giocatore_successivo.posizioni_vittoria ):
				#aumento nuovamente il giocatore attuale
				self.giocatore_attuale += 1
				self.giocatore_attuale %= len(self.partita_attuale.lista_giocatori)
				giocatore_successivo = self.partita_attuale.lista_giocatori[self.giocatore_attuale]
			#adesso mostro il colore nella label apposta
			self.grafica_ui.label_giocatore_attuale.setText(f'È il turno del giocatore {giocatore_successivo.colore}')
			#svuoto le ultime mosse del giocatore
			self.lista_ultime_mosse[giocatore_successivo.id_giocatore].clear()
			#adesso decido se aspettare oppure richiamare il thread che permette di calcolare la mossa successiva del giocatore
			if giocatore_successivo.tipo_giocatore == const_numero_giocatore_umano:
				pass
			elif giocatore_successivo.tipo_giocatore == const_numero_giocatore_computer:
				#per il momento sollevo un'eccezione
				#raise Exception('Giocatore computer ancora non ammesso')
				self.thread_attesa_mossa_pc = ThreadGrafica.ThreadGrafica(grafica=self)
				self.thread_attesa_mossa_pc.finished.connect(self.termina_turno)
				self.thread_attesa_mossa_pc.start()
				#self.thread_attesa_mossa_pc.run()
			else:
				#sollevo eccezione
				raise Exception('Giocatore non riconosciuto in turno successivo')
		#al termine aggiorno classifica e schema
		self.mostra_lista_giocatori()
		self.aggiorna_schema_mostrato()

						




	
