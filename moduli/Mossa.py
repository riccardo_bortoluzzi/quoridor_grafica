#classe per definire la mossa

from __future__ import annotations
from typing import Tuple, Union

const_tipologia_mossa_muro = 0
const_tipologia_mossa_spostamento = 1

class Mossa:
	spostamento : Tuple[ Tuple[int, int], Tuple[int, int] ]
	muro: Tuple[int, int]
	tipologia_mossa : int

	def __init__(self, spostamento:Union[Tuple[ Tuple[int, int], Tuple[int, int] ], None] = None, muro : Union[Tuple[int, int], None] = None) -> None:
		if spostamento is not None:
			#allora ho messo lo spostamento
			self.tipologia_mossa = const_tipologia_mossa_spostamento
			self.spostamento = spostamento
		elif muro is not None:
			#altra tipologia
			self.tipologia_mossa = const_tipologia_mossa_muro
			self.muro = muro
		#else:
		#	raise Exception('Mossa inserita non valida')