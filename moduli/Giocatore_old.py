#script che contiene la classe per il giocatore del quoridor

from __future__ import annotations
from typing import List, Tuple, Union

from moduli.Schema_old import const_numero_casella_vuota, const_numero_casella_muro

class Giocatore:
	posizione : Tuple[int, int]
	posizioni_vittoria : List[ Tuple[int, int] ]
	colore: str
	id_giocatore : int
	flag_movimento_diagonale : bool #flag che indica se ci si puo muovere in diagonale
	#flag_saltare_muri : bool #flag per indicare se si possono saltare i muri
	possibili_direzioni : List[Tuple[int, int]] #possibili direzioni di movimento
	dim_v_schema : int
	dim_o_schema : int

	def __init__(self) -> None:
		#nuovo oggetto nullo utile per fare la copia
		pass

	def popola_da_parametri(self, posizione_partenza:Tuple[int, int], colore:str, id_giocatore:int, flag_movimento_diagonale:bool, dim_v_schema:int, dim_o_schema:int, possibili_direzioni:List[Tuple[int, int]]) -> None:
		self.posizione = posizione_partenza
		self.id_giocatore = id_giocatore
		self.colore = colore
		self.flag_movimento_diagonale = flag_movimento_diagonale
		self.dim_o_schema = dim_o_schema
		self.dim_v_schema = dim_v_schema
		#adesso vado a popolarmi le direzioni di movimento

	def __eq__(self, g2: Giocatore) -> bool:
		return self.id_giocatore == g2.id_giocatore

	def calcola_percorso_piu_corto()

	'''
	#nello schema devo mettere i giocatori che hanno vinto e i giocatori subito adiacenti a quello attuale
	def calcola_percorso_piu_corto_per_vincere(self, schema:List[List[int]]) -> Union[List[Tuple[int, int]], None]:
		#metodo per calcolare le mosse che servirebbero per vincere, ritorna None se non trova nessuna strada
		#per prima cosa verifico se sono gia arrivato
		if self.posizione in self.posizioni_vittoria:
			return [] #non ho necessita di fare mosse per vincere
		#mi preparo la lista che mi riassume tutte le posizioni gia analizzate
		lista_posizioni_analizzate = [self.posizione]
		#adesso mi preparo la lista dei percorsi
		lista_nuovi_percorsi = [ [self.posizione] ]
		while len(lista_nuovi_percorsi) > 0:
			#modifico le liste in modod da trovare tutti i percorsi gia visti
			percorsi_trovati_precedenti = lista_nuovi_percorsi[:]
			lista_nuovi_percorsi = []
			for percorso_trovato in percorsi_trovati_precedenti:
				(r_ultima, c_ultima) = percorso_trovato[-1]
				#provo ad espandere
				for (dr, dc) in self.possibili_direzioni:
					#per prima cosa verifico se posso fare quella mossa
					r_nuova = r_ultima + dr
					c_nuova = c_ultima + dc
					if (0 <= r_nuova < self.dim_v_schema) and ( 0<= c_nuova < self.dim_o_schema ) and ((r_nuova, c_nuova) not in lista_posizioni_analizzate):
						#allora e accettabile e non e ancora stata analizzata
						#adesso verifico se quella casella nello schema e libera
						valore_casella_schema = schema[r_nuova][c_nuova]
						if valore_casella_schema == const_numero_casella_vuota:
							flag_trovato = True
							flag_vuoto = True
						else:
							#non e libera, quindi vado a cercare se devo saltare giocatori o eventualmente muri nella stessa direzione
							flag_trovato = False
							flag_vuoto = False
							while not(flag_trovato):
								#vedo se e una casella che posso saltare perche e di un altro giocatore
								if (valore_casella_schema >= 0) or ((valore_casella_schema == const_numero_casella_muro) and self.flag_saltare_muri):
									#ce un giocatore in quella casella oppure e un muro e posso saltarla
									r_nuova += dr
									c_nuova += dc
									#verifico se sono ancora dentro lo schema e non e gia stata analizzata
									if (0 <= r_nuova < self.dim_v_schema) and ( 0<= c_nuova < self.dim_o_schema ) and ((r_nuova, c_nuova) not in lista_posizioni_analizzate):
										#in tal caso aggiorno il valore
										valore_casella_schema = schema[r_nuova][c_nuova]
									else:
										#sono andato fuori schema, non posso continuare
										flag_trovato = True
									#al termine di questo ricomincio il while
									continue
								elif valore_casella_schema == const_numero_casella_vuota:
									#ho trovato una casella nuova
									flag_trovato = True
									flag_vuoto = True
								else:
									#significa che non posso continuare perche trovo un muro e non posso superarlo
									flag_trovato = True
						#adesso verifico se la casella di destinazione e vuota
						if flag_vuoto:
							#ho trovato un nuovo percorso
							nuova_casella = (r_nuova, c_nuova)
							nuovo_percorso = percorso_trovato + [nuova_casella]
							#verifico se sono arrivato a soluzione
							if (nuova_casella in self.posizioni_vittoria):
								#ho trovato un percorso che mi permetterebbe di vincere
								return nuovo_percorso
							else:
								#altrimenti devo aggiungerlo ai nuovi percorsi
								lista_nuovi_percorsi.append(nuovo_percorso)
								#aggiungo la casella attuale
								lista_posizioni_analizzate.append(nuova_casella)
		#se non ci sono nuovi percorsi significa che non esiste alcun percorso che mi permetterebbe di vincere
		return None

	def calcola_lista_percorsi_piu_corti_per_vincere(self, schema:List[List[int]]) -> List[List[Tuple[int, int]]]:
		#metodo per calcolare le mosse che servirebbero per vincere, ritorna None se non trova nessuna strada
		#per prima cosa verifico se sono gia arrivato
		if self.posizione in self.posizioni_vittoria:
			return [[]] #non ho necessita di fare mosse per vincere
		#mi preparo la lista che mi riassume tutte le posizioni gia analizzate
		lista_posizioni_analizzate = [self.posizione]
		lista_percorsi_migliori = []
		#adesso mi preparo la lista dei percorsi
		lista_nuovi_percorsi = [ [self.posizione] ]
		while (len(lista_nuovi_percorsi) > 0) and (len(lista_percorsi_migliori) == 0):
			#modifico le liste in modod da trovare tutti i percorsi gia visti
			percorsi_trovati_precedenti = lista_nuovi_percorsi[:]
			lista_nuovi_percorsi = []
			for percorso_trovato in percorsi_trovati_precedenti:
				(r_ultima, c_ultima) = percorso_trovato[-1]
				#provo ad espandere
				for (dr, dc) in self.possibili_direzioni:
					#per prima cosa verifico se posso fare quella mossa
					r_nuova = r_ultima + dr
					c_nuova = c_ultima + dc
					if (0 <= r_nuova < self.dim_v_schema) and ( 0<= c_nuova < self.dim_o_schema ) and ((r_nuova, c_nuova) not in lista_posizioni_analizzate):
						#allora e accettabile e non e ancora stata analizzata
						#adesso verifico se quella casella nello schema e libera
						valore_casella_schema = schema[r_nuova][c_nuova]
						if valore_casella_schema == const_numero_casella_vuota:
							flag_trovato = True
							flag_vuoto = True
						else:
							#non e libera, quindi vado a cercare se devo saltare giocatori o eventualmente muri nella stessa direzione
							flag_trovato = False
							flag_vuoto = False
							while not(flag_trovato):
								#vedo se e una casella che posso saltare perche e di un altro giocatore
								if (valore_casella_schema >= 0) or ((valore_casella_schema == const_numero_casella_muro) and self.flag_saltare_muri):
									#ce un giocatore in quella casella oppure e un muro e posso saltarla
									r_nuova += dr
									c_nuova += dc
									#verifico se sono ancora dentro lo schema e non e gia stata analizzata
									if (0 <= r_nuova < self.dim_v_schema) and ( 0<= c_nuova < self.dim_o_schema ) and ((r_nuova, c_nuova) not in lista_posizioni_analizzate):
										#in tal caso aggiorno il valore
										valore_casella_schema = schema[r_nuova][c_nuova]
									else:
										#sono andato fuori schema, non posso continuare
										flag_trovato = True
									#al termine di questo ricomincio il while
									continue
								elif valore_casella_schema == const_numero_casella_vuota:
									#ho trovato una casella nuova
									flag_trovato = True
									flag_vuoto = True
								else:
									#significa che non posso continuare perche trovo un muro e non posso superarlo
									flag_trovato = True
						#adesso verifico se la casella di destinazione e vuota
						if flag_vuoto:
							#ho trovato un nuovo percorso
							nuova_casella = (r_nuova, c_nuova)
							nuovo_percorso = percorso_trovato + [nuova_casella]
							#verifico se sono arrivato a soluzione
							if (nuova_casella in self.posizioni_vittoria):
								#ho trovato un percorso che mi permetterebbe di vincere
								lista_percorsi_migliori.append(nuovo_percorso)
							#lo aggiungo ai percorsi successivi
							lista_nuovi_percorsi.append(nuovo_percorso)
							#aggiungo la casella attuale
							lista_posizioni_analizzate.append(nuova_casella)
		#se non ci sono nuovi percorsi significa che non esiste alcun percorso che mi permetterebbe di vincere
		return lista_percorsi_migliori
	'''


	def calcola_percorsi_minori(self, schema:np.ndarray, flag_termina_primo_percorso:bool, oggetto_schema:Schema) -> List[List[np.ndarray]]:
		#metodo per calcolare i percorsi minimi per arrivare alla soluzione
		#per prima cosa verifico se sono arrivato alla soluzione
		if self.posizione in self.posizioni_vittoria:
			return [ [] ]
		#altrimenti mi recupero le dimensioni dello schema
		(dim_v, dim_o) = schema.shape
		#mi calcolo l'array delle caselle libere
		array_libere = np.array( np.where( schema != const_casella_muro ) )
		#mi faccio un array delle posizioni trovate
		lista_posizioni_analizzate = [ ]
		lista_posizioni_analizzate_questo_ciclo = [self.posizione]
		lista_percorsi_minimi_trovati = []
		lista_percorsi_successivi = [ [self.posizione] ]
		lunghezza_successiva_percorso = 1
		while (len(lista_percorsi_minimi_trovati) == 0) and (len(lista_percorsi_successivi) > 0):
			lista_percorsi_precedenti = lista_percorsi_successivi[:]
			#svuoto i succcessivi
			lista_percorsi_successivi = []
			#aggiorno la lista delle posizioni gia visitate
			lista_posizioni_analizzate.extend(lista_posizioni_analizzate_questo_ciclo)
			lista_posizioni_analizzate_questo_ciclo = []
			lunghezza_successiva_percorso += 1
			#for percorso in lista_percorsi_precedenti: #sorted(lista_percorsi_precedenti, key=lambda x: len(x)): #prendo prima quelli piu corti
			indice_percorso_precedenti = 0
			while indice_percorso_precedenti < len(lista_percorsi_precedenti):
				#mi calcolo il percorso da analizzare
				percorso_da_analizzare = lista_percorsi_precedenti[indice_percorso_precedenti]
				#mi calcolo la testa e la coda
				testa_percorso = percorso_da_analizzare[ min(1, len(percorso_da_analizzare) -1) ]
				coda_percorso = percorso_da_analizzare[1]
				#mi ricavo le caselle succesive
				for successiva_posizione in oggetto_schema.calcola_caselle_accettabili_successive(schema=schema, casella=coda_percorso, array_libere=array_libere):
					#per prima cosa verifico se non e presente in quelle gia analizzate
					if successiva_posizione not in lista_posizioni_analizzate:
						#la aggiungo subito 
						lista_posizioni_analizzate.append(successiva_posizione)
						#mi creo il nuovo percorso
						nuovo_percorso = percorso_da_analizzare + [successiva_posizione]

				
				#al termine incremento l'indice
				indice_percorso_precedenti += 1

						