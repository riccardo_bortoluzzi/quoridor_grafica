#script che contiene le funzioni per gestire lo schema 

from __future__ import annotations

from typing import List, Tuple
from moduli.Giocatore_old import Giocatore

const_numero_casella_vuota = -1
const_numero_casella_muro = -2

class Schema:
	dim_v : int
	dim_o : int
	flag_movimento_diagonale : bool #flag che indica se ci si puo muovere in diagonale
	possibili_direzioni : List[Tuple[int, int]] #possibili direzioni di movimento
	
	'''
	def calcola_mosse_diponibili(self, schema:List[List[int]], posizione:Tuple[int, int], flag_salta_giocatori:bool) -> List[ Tuple[int, int] ]:
		#metodo per calcolare tutte le mosse possibili dato un determinato schema, il flag permette di saltare i giocatori (applicabile solo se alla prima mossa)
		lista_caselle_disponibili = []
		(r_ultima, c_ultima) = posizione
		for (dr, dc) in self.possibili_direzioni:
			#per prima cosa verifico se posso fare quella mossa
			r_nuova = r_ultima + dr
			c_nuova = c_ultima + dc
			if (0 <= r_nuova < self.dim_v) and ( 0<= c_nuova < self.dim_o ):
				#allora e accettabile e non e ancora stata analizzata
				#adesso verifico se quella casella nello schema e libera
				valore_casella_schema = schema[r_nuova][c_nuova]
				if valore_casella_schema == const_numero_casella_vuota:
					flag_trovato = True
					flag_vuoto = True
				else:
					#non e libera, quindi vado a cercare se devo saltare giocatori o eventualmente muri nella stessa direzione
					flag_trovato = False
					flag_vuoto = False
					while not(flag_trovato):
						#vedo se e una casella che posso saltare perche e di un altro giocatore
						if (valore_casella_schema >= 0) or ((valore_casella_schema == const_numero_casella_muro) and self.flag_saltare_muri):
							#ce un giocatore in quella casella oppure e un muro e posso saltarla
							r_nuova += dr
							c_nuova += dc
							#verifico se sono ancora dentro lo schema e non e gia stata analizzata
							if (0 <= r_nuova < self.dim_v_schema) and ( 0<= c_nuova < self.dim_o_schema ) and ((r_nuova, c_nuova) not in lista_posizioni_analizzate):
								#in tal caso aggiorno il valore
								valore_casella_schema = schema[r_nuova][c_nuova]
							else:
								#sono andato fuori schema, non posso continuare
								flag_trovato = True
							#al termine di questo ricomincio il while
							continue
						elif valore_casella_schema == const_numero_casella_vuota:
							#ho trovato una casella nuova
							flag_trovato = True
							flag_vuoto = True
						else:
							#significa che non posso continuare perche trovo un muro e non posso superarlo
							flag_trovato = True
				#adesso verifico se la casella di destinazione e vuota
				if flag_vuoto:
					#ho trovato un nuovo percorso
					nuova_casella = (r_nuova, c_nuova)
					nuovo_percorso = percorso_trovato + [nuova_casella]
					#verifico se sono arrivato a soluzione
					if (nuova_casella in self.posizioni_vittoria):
						#ho trovato un percorso che mi permetterebbe di vincere
						return nuovo_percorso
					else:
						#altrimenti devo aggiungerlo ai nuovi percorsi
						lista_nuovi_percorsi.append(nuovo_percorso)
						#aggiungo la casella attuale
						lista_posizioni_analizzate.append(nuova_casella)
	'''

	def calcola_percorso_piu_corto(self, schema:List[List[int]], partenza:Tuple[int, int], lista_celle_arrivo:List[Tuple[int, int]]):
		

def calcola_schema_con_giocatori(schema:List[List[int]], lista_giocatori:List[Giocatore]):
	#semplicemente calcola un nuovo schema inserendo anche i numeri dei giocatori
	nuovo_schema = [riga[:] for riga in schema]
	for player in lista_giocatori:
		(r,c) = player.posizione
		nuovo_schema[r][c] = player.id_giocatore
	return nuovo_schema