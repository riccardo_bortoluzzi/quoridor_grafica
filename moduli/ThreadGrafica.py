#script che contiene la classe per gestire il thread per la grafica con l'emissione dei segnali

from __future__ import annotations

import time

from PyQt5.QtCore import QThread, pyqtSignal
import moduli.MainWindowQuoridor as MainWindowQuoridor
from moduli.Mossa import const_tipologia_mossa_muro, const_tipologia_mossa_spostamento
from moduli.Giocatore import const_numero_muri_restanti_infinito

class ThreadGrafica(QThread):
	grafica_quoridor : MainWindowQuoridor.MainWindowQuoridor
	finished:pyqtSignal = pyqtSignal()

	def __init__(self, grafica:MainWindowQuoridor.MainWindowQuoridor) -> None:
		super().__init__()
		self.grafica_quoridor = grafica

	def run(self) -> None:
		#mi ricavo il giocatoire in turno
		print('inizio calcolo mossa thread')
		partita_iniziale = self.grafica_quoridor.partita_attuale
		id_giocatore_in_turno = self.grafica_quoridor.giocatore_attuale
		#calcolo l'attuale profondita
		profondita = self.grafica_quoridor.grafica_ui.spinBox_profondita.value()
		#calcolo la mossa del giocatore
		mossa_scelta = partita_iniziale.lista_giocatori[id_giocatore_in_turno].calcola_mossa_migliore_min_max(schema_partenza=partita_iniziale.schema_attuale, lista_giocatori=partita_iniziale.lista_giocatori, profondita=profondita, flag_controlla_sempre_muri=partita_iniziale.flag_analizza_muri, modalita_prediligi=partita_iniziale.tipologia_priorita,flag_inverso=partita_iniziale.flag_inverso)
		#adesso stabilisco se e uno spostamento o un muro
		if mossa_scelta.tipologia_mossa == const_tipologia_mossa_spostamento:
			#devo cambiare la lista dei giocatori
			partita_iniziale.lista_giocatori = partita_iniziale.lista_giocatori.copy()
			nuovo_giocaotre = partita_iniziale.lista_giocatori[id_giocatore_in_turno].copy(nuova_posizione=mossa_scelta.spostamento[1], muri_restanti=partita_iniziale.lista_giocatori[id_giocatore_in_turno].muri_restanti, flag_tutti_campi=True)
			partita_iniziale.lista_giocatori[id_giocatore_in_turno] = nuovo_giocaotre
		elif mossa_scelta.tipologia_mossa == const_tipologia_mossa_muro:
			#devo modifciare solo lo shcmea
			partita_iniziale.schema_attuale = partita_iniziale.schema_attuale.inserisci_muro(posizione_muro=mossa_scelta.muro)
			if partita_iniziale.numero_muri_iniziali != const_numero_muri_restanti_infinito:
				#e la lista aggiornata dei giocatori
				partita_iniziale.lista_giocatori = partita_iniziale.lista_giocatori.copy()
				nuovo_giocaotre = partita_iniziale.lista_giocatori[id_giocatore_in_turno].copy(nuova_posizione=partita_iniziale.lista_giocatori[id_giocatore_in_turno].posizione, muri_restanti=partita_iniziale.lista_giocatori[id_giocatore_in_turno].muri_restanti-1, flag_tutti_campi=True)
				partita_iniziale.lista_giocatori[id_giocatore_in_turno] = nuovo_giocaotre
		else:
			raise Exception('Tipologia mossa non valida')
		#queste operazioni le eseguo se la partita e ancora la stessa
		if self.grafica_quoridor.partita_attuale == partita_iniziale:
			#vado a popolare la lisat di ultima mossa
			self.grafica_quoridor.lista_ultime_mosse[id_giocatore_in_turno].clear()
			if mossa_scelta.tipologia_mossa == const_tipologia_mossa_spostamento:
				self.grafica_quoridor.lista_ultime_mosse[id_giocatore_in_turno].extend( [ (r+1, c+1) for (r,c) in mossa_scelta.spostamento] ) #devo mettere il +1 perche le coordinate sono in funzione dello schma
			elif mossa_scelta.tipologia_mossa == const_tipologia_mossa_muro:
				self.grafica_quoridor.lista_ultime_mosse[id_giocatore_in_turno].append( (mossa_scelta.muro[0]+1, mossa_scelta.muro[1]+1) )
			print('fine calcolo mossa thread')
			#termino il thread
			self.finished.emit()