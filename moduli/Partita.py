#script che contiene la classe per gestyire e memorizzare una partita

from __future__ import annotations

import numpy as np

from typing import List
import time
from moduli.Giocatore import Giocatore,const_numero_giocatore_disabilitato
from moduli.Schema import Schema, const_casella_vuota


#creo le liste dei colori dei giocatori e i relativi dizionari
const_lista_colori = ['Rosso', 'Verde', 'Azzurro', 'Fucsia']
const_lista_colori_inglesi = ['red', 'green', 'cyan', 'magenta']
const_diz_associazione_colori = dict( zip(const_lista_colori, const_lista_colori_inglesi) )

class Partita:
	tempo_inizio : float
	schema_attuale : Schema
	lista_giocatori : List[Giocatore]
	flag_analizza_muri : bool
	tipologia_priorita:int #se dare priorita ai muri, agli spostamenti o a niente
	is_partita_finita:bool
	numero_muri_iniziali : int
	flag_inverso:bool #se true vince l'ultimo giocatore che resta in gioco

	def __init__(self, dim_v:int, 
					   dim_o:int, 
					   flag_analizza_muri:bool, #flag che indica se analizzare sempre i muri
					   numero_muri_per_giocatore:int, #numero dei muri che ogni giocatore puo inserire
					   tipo_priorita:int, #numero che indica se privilegiare i muri, gli spostamenti o niente
					   tipologia_giocatore_1:int, #tipologia dei vari giocatori
					   tipologia_giocatore_2:int,
					   tipologia_giocatore_3:int,
					   tipologia_giocatore_4:int,
					   flag_inverso	:bool
					   ) -> None:
		#ci metto il tempo di creazione 
		self.tempo_inizio = time.time()
		self.is_partita_finita = False
		self.flag_inverso = flag_inverso
		#metto gli altri attributi
		self.flag_analizza_muri = flag_analizza_muri
		self.tipologia_priorita = tipo_priorita
		self.numero_muri_iniziali = numero_muri_per_giocatore
		#mi creo lo schema
		schema_np = np.ones((dim_v, dim_o), int) * const_casella_vuota
		self.schema_attuale = Schema(schema=schema_np)
		self.schema_attuale.popola_lista_direzioni(flag_diagonali=False)
		#adesso mi creo la lista dei giocatori
		self.lista_giocatori = []
		lista_tipologie_giocatori = [tipologia_giocatore_1, tipologia_giocatore_2, tipologia_giocatore_3, tipologia_giocatore_4]
		lista_celle_partenza_giocatori = [(dim_v-1, dim_o//2), (dim_v // 2, dim_o-1), (0, dim_o//2), (dim_v//2, 0)]
		id_giocatore_attuale = 0
		#adesso itero tra tutte le tipologie
		for i in range(len(lista_tipologie_giocatori)):
			#verifico se la tipologia dice che il giocatore e abilitato
			if lista_tipologie_giocatori[i] != const_numero_giocatore_disabilitato:
				#allora posso inserirlo
				nuovo_giocatore = Giocatore()
				#popolo i parametri
				if lista_tipologie_giocatori[i] == const_numero_giocatore_disabilitato:
					nuovo_giocatore.popola_giocatore_disabilitato(id_giocatore=id_giocatore_attuale,
																	colore=const_lista_colori[i])
				else:
					#e un giocatore non disabilitato
					nuovo_giocatore.popola_attributi(id_giocatore=id_giocatore_attuale,
														colore=const_lista_colori[i],
														dim_o=dim_o,
														dim_v=dim_v, 
														muri_restanti=numero_muri_per_giocatore,
														posizione_partenza=lista_celle_partenza_giocatori[i],
														tipologia_giocatore=lista_tipologie_giocatori[i])
					self.lista_giocatori.append(nuovo_giocatore) #messo qui per considerare solo i giocatori attualmente in gioco
					id_giocatore_attuale += 1
		

	def __eq__(self, p2: Partita) -> bool:
		return self.tempo_inizio == p2.tempo_inizio