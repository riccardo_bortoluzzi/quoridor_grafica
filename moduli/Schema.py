#script che contiene la classe per gestire lo schema, come ad esempio le mosse disponibili

from __future__ import annotations
from typing import Any, Generator, List, Tuple

import numpy as np

import moduli.Giocatore as Giocatore



const_casella_vuota = -1
const_casella_muro = -2

class Schema:
	lista_direzioni : List[Tuple[int, int]]
	schema_attuale : np.ndarray

	def __init__(self, schema:np.ndarray) -> None:
		self.schema_attuale = schema
		#self.lista_direzioni = []

	def copy(self) -> Schema:
		#metodo per eseguire la copia dello schema che fa copia di array e la lista delle direzioni prende il riferimento a quella attuale
		nuovo_schema = Schema(schema=self.schema_attuale.copy())
		nuovo_schema.lista_direzioni = self.lista_direzioni
		return nuovo_schema

	def popola_lista_direzioni(self, flag_diagonali:bool) -> None:
		#mi creo la lista delle possibili direzioni
		lista_possibili_direzioni = [ (1,0), (-1, 0), (0,1), (0, -1) ]
		#valuto se devo mettere anche le diagonali
		if flag_diagonali:
			lista_possibili_direzioni.extend([ (1,1), (1,-1), (-1,1), (-1-1) ])
		self.lista_direzioni = lista_possibili_direzioni

	def calcola_caselle_accettabili_successive(self, casella:Tuple[int, int]) -> Generator[Tuple[int, int], Any, Any]:
		#metodo per calcolare le caselle successive dato e schema e casella di partenza
		#mi ricavo le dimensioni
		(dim_v, dim_o) = self.schema_attuale.shape
		#mi calcolo dove sono i muri
		#array_libere = np.array( np.where( schema != const_casella_muro ) )
		for delta in self.lista_direzioni:
			#mi calcolo la nuova casella
			nuova_casella = (casella[0] + delta[0], casella[1] + delta[1])
			(r_n, c_n) = nuova_casella
			#verifico intanto se e dentro lo schema  e se non e un muro la nuova casella
			if (0<= r_n < dim_v) and (0 <= c_n < dim_o) and (self.schema_attuale[r_n, c_n] != const_casella_muro):
				#allora e ancora dentro lo schema e non e un muro
				yield nuova_casella

	def inserisci_giocatori_in_schema(self, lista_giocatori:List[Giocatore.Giocatore]) -> Schema:
		#metodo per inserire i giocatori nello schema e ritornare il nuovo schema
		nuovo_schema = self.copy()
		for g in lista_giocatori:
			#inserisco solo i giocatori che non sono ancora arrivati
			if g.posizione not in g.posizioni_vittoria:
				(r,c) = g.posizione
				nuovo_schema.schema_attuale[r,c] = g.id_giocatore
		return nuovo_schema

	def inserisci_muro(self, posizione_muro:Tuple[int, int]) -> Schema:
		#metodo per inserire un muro nel nuovo schema
		nuovo_schema = self.copy()
		(r, c) = posizione_muro
		nuovo_schema.schema_attuale[r,c] = const_casella_muro
		return nuovo_schema

	def stampa_schema(self, lista_giocaotri:List[Giocatore.Giocatore]):
		from termcolor import colored
		#metodo per stampare lo schema a console
		carattere_vuoto = ' '
		carattere_muro = colored(' ', 'red', 'on_red')#'#'
		(dim_v, dim_o) = self.schema_attuale.shape
		matrice_stringhe = [ [ carattere_vuoto for c in range(dim_o) ] for r in range(dim_v) ]
		#adesso inserisco i muri
		for r in range(dim_v):
			for c in range(dim_o):
				if self.schema_attuale[r,c] == const_casella_muro:
					matrice_stringhe[r][c] = carattere_muro
		#adesso ci metto i giocatori
		for g in lista_giocaotri:
			if g.tipo_giocatore != Giocatore.const_numero_giocatore_disabilitato:
				(r_g, c_g) = g.posizione
				matrice_stringhe[r_g][c_g] = g.colore#str(g.id_giocatore)
		#adesso sono pronto per stampare
		print('\nNuovo schema:')
		#chiudo lo schema dentro un quadrato
		#print('-'*(dim_o*2 +1))
		#print('-'*(dim_o +2))
		#print((colored(' ', 'white', 'on_white'))*(dim_o +2))
		print(lista_giocaotri[2].colore *(dim_o +2))
		for riga in matrice_stringhe:
			#print('|' + ''.join(riga) + '|')
			#print(colored(' ', 'white', 'on_white') + ''.join(riga) + colored(' ', 'white', 'on_white'))
			print(lista_giocaotri[3].colore + ''.join(riga) + lista_giocaotri[1].colore)
		#al termine stampo la riga finale
		#print('-'*(dim_o * 2 +1))
		#print('-'*(dim_o + 2))
		#print((colored(' ', 'white', 'on_white'))*(dim_o +2))
		print(lista_giocaotri[0].colore *(dim_o +2))
