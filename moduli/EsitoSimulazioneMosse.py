#script che contiene la classe per descrivere l'esito della simulazione delle mosse

from __future__ import annotations
from typing import List, Tuple, Union

import moduli.Schema 
import moduli.Giocatore as Giocatore
from moduli.Mossa import Mossa

class EsitoSimulazioneMosse(Mossa):
	#schema : Schema
	#lista_giocatori : List[Giocatore]
	lista_lunghezze_percorsi_minimi : List[int]

	def __init__(self, spostamento: Union[Tuple[Tuple[int, int], Tuple[int, int]], None] = None, muro: Union[Tuple[int, int], None] = None) -> None:
		super().__init__(spostamento=spostamento, muro=muro)

	def calcola_lunghezze_percorsi(self, schema: moduli.Schema.Schema, lista_giocatori:List[Giocatore.Giocatore]):
		#metodo per calcolare le lunghezze 
#		self.lista_lunghezze_percorsi_minimi = [ (len(g.calcola_percorso_minimo(schema=schema, lista_giocatori=lista_giocatori))) if g.tipo_giocatore != moduli.Giocatore.const_numero_giocatore_disabilitato else 0 for g in lista_giocatori ]
		lista_percorsi_minimi = [ g.calcola_percorso_minimo(schema=schema, lista_giocatori=lista_giocatori ) for g in lista_giocatori if (g.tipo_giocatore != Giocatore.const_numero_giocatore_disabilitato) ]
		self.lista_lunghezze_percorsi_minimi = [ (0 if p is None else len(p)) for p in lista_percorsi_minimi ]

	def calcola_massima_differenza_percorsi(self, id_giocatore_analisi:int, lista_id_altri_giocatori:List[int], flag_inverso:bool) -> int:
		#metodo che clacola la differenza minima di mosse tra il giocaore in analisi e gli altri giocatori
		lunghezza_percorso_giocatore_analisi = self.lista_lunghezze_percorsi_minimi[id_giocatore_analisi]
		lista_differenze = [ lunghezza_percorso_giocatore_analisi - self.lista_lunghezze_percorsi_minimi[i] for i in lista_id_altri_giocatori ]
		if flag_inverso:
			return min(lista_differenze) #se cerco di perdere se arrivo significa che devo tornare il minimo (se negativo significa che c'è qualcuno che ha di sicuro piu passi da fare rispetto al giocatore in questione) [fare esempio con carta e penna con lunghezze di 7,5,7,10]
		else:
			return max(lista_differenze)
			#prendo la massima distanza perche è la lunghezza di quello in analisi - il percorso di lungheza minima
