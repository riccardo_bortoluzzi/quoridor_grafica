#!.venv/bin/python3

#script per gestire la grafica del quoridor

import moduli.MainWindowQuoridor as MainWindowQuoridor

import sys, traceback

try:

	from PyQt5 import QtCore, QtGui, QtWidgets

	if __name__ == '__main__':
		app = QtWidgets.QApplication(sys.argv)

		grafica = MainWindowQuoridor.MainWindowQuoridor()
		grafica.show()

		sys.exit(app.exec_())
		input('Premere Invio per chiudere la finestra')
except Exception as e:
	etype, value, tb = sys.exc_info()
	stringa_errore = ''.join(traceback.format_exception(etype, value, tb))
	print(stringa_errore)
	input('Si è verificato un errore. inviare le scritte qui sopra a riccardo.bortoluzzi96+quoridor@gmail.com')
