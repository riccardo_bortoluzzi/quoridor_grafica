# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['main_quoridor_grafica.py'],
             pathex=[],
             binaries=[],
             datas=[('.\\immagini_pedine\\pedina_black.png', 'immagini_pedine'), ('.\\immagini_pedine\\pedina_blue.png', 'immagini_pedine'), ('.\\immagini_pedine\\pedina_green.png', 'immagini_pedine'), ('.\\immagini_pedine\\pedina_red.png', 'immagini_pedine'), ('.\\immagini_pedine\\pedina_cyan.png', 'immagini_pedine'), ('.\\immagini_pedine\\pedina_magenta.png', 'immagini_pedine')],
             hiddenimports=['typing', 'random', 'os', 'numpy', 'time', 'termcolor', 'PyQt5', 'moduli.EsitoSimulazioneMosse', 'moduli.Giocatore', 'moduli.GraficaQuoridor', 'moduli.MainWindowQuoridor', 'moduli.Mossa', 'moduli.Partita', 'moduli.Schema', 'moduli.ThreadGrafica'],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=True,
             win_private_assemblies=True,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,  
          [],
          name='Quoridor',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None , version='version_file.txt', icon='icona\\icona.ico')
