# Quoridor


Gioco del quoridor implementato in python3 con grafica Qt

## Installazione

Per installare le varie dipendenze:

```
pip3 install --upgrade --requirements requirements.txt
```

Per eseguire il gioco:
```
python3 main_quoritodr_grafica.py
```

Volendo c'è anche il file `main_quoridor_simulazione_terminale.py` che permette una simulazione di un computer che gioca contro se stesso con una grafica da terminale colorata.

All'interno della cartella `compilato_windows` è presente l'eseguibile `Quoridor.exe` autoavviante per Windows.

## Scopo del gioco

Lo scopo di ogni giocatore è riuscire ad arrivare in una casella attaccata ad una di quelle colorate con lo stesso colore della propria pedina.

Per farlo ci si muove a turno in senso orario, partendo dal giocatore rosso.

Le mosse si dividono in 2 tipologie:

1. Uno spostamento verticale o orizzontale della propria pedina, per farlo è necessario cliccare prima sulla pedina e poi sulla casella in cui ci si vuole muovere. Nel caso ci sia una pedina di un altro giocatore in una delle caselle possibili per lo spostamento, sarà possibile scavalcare quel giocatore.
2. L'inserimento di un muro in una posizione dello schema di gioco. Ogni muro non può essere posizionato sopra un altro muro, non può essere posizionato sopra la pedina di un altro giocatore e deve comunque garantire a tutti i giocatori almeno una strada per raggiungere la propria meta. Per inserire un muro, se ancora disponibili, si deve cliccare 2 volte su una casella dello schema, la prima volta si illuminerà di giallo, cliccandoci la seconda volta verrà inserito il muro.


Per una spiegazione più dettagliata del gioco, consultare la pagina di wikipedia relativa al [Quoridor](https://it.wikipedia.org/wiki/Quoridor) da cui questo gioco è stato tratto (al contrario di quanto presente nella pagina wikipedia, qui i muri si inseriscono direttamente nelle caselle e non in mezzo tra 2 caselle).

## Come pensa il computer
Il computer analizza le possibili mosse da compiere e valuta il guadagnao di una mossa confrontando la lunghezza del suo percorso minimo per vincere con la lunghezza dei percorsi degli altri giocatori.

## Parametri di gioco

- Numero righe: numero di righe dello schema di gioco.
- Numero colonne: numero di colonen dello schema di gioco.
- Privilegia: Il computer, nel caso in cui valuti che due mosse di tipologia diversa abbiano un guadagno positivo, può privilegiare l'inserimento di un muro, uno spostamento oppure prendere casualmente uno dei 2.
- La tipologia dei vari giocatori (se controllati dall'utente, dal computer o non presenti).
	- Il giocatore Rosso partirà dalla casella centrale della riga più in basso e dovrà raggiungere la riga in alto dello schema;
	- Il giocatore Verde partirà dalla casella centrale della colonna a destra e dovrà raggiungere la colonna a sinistra dello schema;
	- Il giocatore Rosso partirà dalla casella centrale della riga più in alto e dovrà raggiungere la riga in basso dello schema;
	- Il giocatore Rosso partirà dalla casella centrale della colonna più a sinistra e dovrà raggiungere la colonna a destra dello schema;
- Analizza sempre i muri: indica se il computer debba analizzare sempre la possibilità di inserirre muri nei percorsi degli avversari oppure, nel caso trovasse una mossa spostamento con guadagno positivo, esegua quella mossa.
- Muri per giocatore: Indica quanti muri può inserire ciascun giocatore. Inserire -1 per avere un numero infinito di muri.

## Schermata di gioco
Nella scheramta di gioco è possibile visualizzare la classifica parziale (i giocatori con un asterisco non sono ancora arrivati a destinazione) e i muri restanti per ogni giocatore.

Inoltre è possibile specificare la profondità di analisi delle mosse del computer e la dimensione delle caselle dello schema.


Per qualsiasi problema o segnalazione contattare [riccardo.bortoluzzi96+quoridor@gmail.com](mailto:riccardo.bortoluzzi96+quoridor@gmail.com)
