python -m pip install --upgrade pip
python -m pip install --upgrade -r requirements.txt


pyinstaller --distpath .\compilato_windows ^
            --noconfirm ^
			--clean ^
			--onefile ^
			--name Quoridor ^
			--add-data=".\immagini_pedine\pedina_black.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_blue.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_green.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_red.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_cyan.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_magenta.png;immagini_pedine" ^
			--hiddenimport typing ^
			--hiddenimport random ^
			--hiddenimport os ^
			--hiddenimport numpy ^
			--hiddenimport time ^
			--hiddenimport termcolor ^
			--hiddenimport PyQt5 ^
			--hiddenimport moduli.EsitoSimulazioneMosse ^
			--hiddenimport moduli.Giocatore ^
			--hiddenimport moduli.GraficaQuoridor ^
			--hiddenimport moduli.MainWindowQuoridor ^
			--hiddenimport moduli.Mossa ^
			--hiddenimport moduli.Partita ^
			--hiddenimport moduli.Schema ^
			--hiddenimport moduli.ThreadGrafica ^
			--console ^
			--icon .\icona\icona.ico ^
			--version-file .\version_file.txt ^
			--win-private-assemblies ^
			--win-no-prefer-redirects ^
			main_quoridor_grafica.py

pause